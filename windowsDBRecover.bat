@echo off
set /p pgDumpLocation="Enter pg_restore location:"
set /p port="Specify port of PostgreSql Server:"
set /p user="Enter db login: "
set /p database="Enter db name: "
"%pgDumpLocation%\pg_restore.exe" -p %port% -U %user% -d %database% -v ApartmentsDB.backup
set /p dummy=""