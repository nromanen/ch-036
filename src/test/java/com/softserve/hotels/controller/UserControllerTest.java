package com.softserve.hotels.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.softserve.hotels.configuration.AppConfig;
import com.softserve.hotels.configuration.SecurityConfig;
import com.softserve.hotels.service.UserService;

@Test
@WebAppConfiguration
@ContextConfiguration(classes = { AppConfig.class, SecurityConfig.class })
@TestPropertySource(locations = "classpath:testdb.properties")
public class UserControllerTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    private final UserRequestPostProcessor adminUserStub = user("user@gmail.com")
            .authorities(new SimpleGrantedAuthority("USER"));

    @BeforeClass
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).addFilters(springSecurityFilterChain)
                .build();
    }

    @AfterClass
    public void teardown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    @Transactional
    void testStartPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/1").with(adminUserStub).with(csrf())).andExpect(status().isOk());
    }

}
