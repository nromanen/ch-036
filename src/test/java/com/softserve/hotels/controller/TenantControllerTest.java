package com.softserve.hotels.controller;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;


import java.util.List;

import javax.servlet.Filter;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.User;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.IObjectFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.ObjectFactory;
import org.testng.annotations.Test;
import org.testng.Assert;

import com.softserve.hotels.configuration.AppConfig;
import com.softserve.hotels.configuration.SecurityConfig;
import com.softserve.hotels.dto.CanceledReservation;
import com.softserve.hotels.model.ActionStatus;
import com.softserve.hotels.model.User;
import com.softserve.hotels.model.paymentResult;
import com.softserve.hotels.service.CustomUserDetailsService;
import com.softserve.hotels.service.ReservedService;
import com.softserve.hotels.service.UserService;
import com.softserve.hotels.social.CustomUserDetails;

import static org.powermock.api.support.membermodification.MemberMatcher.method;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.testng.PowerMockObjectFactory;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.mockito.Mockito.*;

@Test
@WebAppConfiguration
@ContextConfiguration(classes = { AppConfig.class, SecurityConfig.class })
@TestPropertySource(locations = "classpath:testdb.properties")
public class TenantControllerTest extends AbstractTransactionalTestNGSpringContextTests {
    
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private ReservedService reservedService;

    @Autowired
    private CustomUserDetailsService userDetailService;
    
    private MockMvc mockMvc;
    
    private CustomUserDetails customUser;
    
    private final UserRequestPostProcessor tenantUserStub = user("user@gmail.com").password("user1")
            .authorities(new SimpleGrantedAuthority("USER"));
    
    

    private Authentication authentication;
    
    private SecurityContext securityContext;
    
    private UserService mockuserService;
    
    @Autowired
    @Mock
    private UserService mockUserService;
    
    @InjectMocks
    private TenantController tenantController;
    
 
    
    
    @BeforeClass
    public void setUp() {
        
        MockitoAnnotations.initMocks(this);
        when(mockUserService.getUserFromContext()).thenReturn(null);
        
        
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).addFilters(springSecurityFilterChain)
                .build();

    }
    
    @AfterClass
    public void teardown() {
        SecurityContextHolder.clearContext();
    }
    
    public void testCancelReservation() throws Exception {
                
        mockMvc.perform(MockMvcRequestBuilders.post("/tenant/unbookApartment")                
        .with(tenantUserStub)
        .with(csrf())
        .param("type", "1")
        .param("idReservation", "1")
        .param("comment", "Because"))
        .andExpect(MockMvcResultMatchers.redirectedUrl("/tenant/activeOrders?count=0"));
        Assert.assertEquals(reservedService.findById(1).getStatus(), ActionStatus.DECLINED_TENANT);
    } 

    
    public void testPaymentDetails() throws Exception {
        
        mockMvc.perform(MockMvcRequestBuilders.post("/tenant/paymentDetails")                
        .with(tenantUserStub)
        .with(csrf())
        .param("id", "1"))
        .andExpect(model().attribute("availablePayments", hasSize(3)));
    } 
}
