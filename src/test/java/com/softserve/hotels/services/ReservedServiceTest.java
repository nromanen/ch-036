package com.softserve.hotels.services;

import javax.transaction.Transactional;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.softserve.hotels.configuration.AppConfig;
import com.softserve.hotels.dto.ActiveOrders;
import com.softserve.hotels.dto.RenterOrdersDto;
import com.softserve.hotels.model.ActionStatus;
import com.softserve.hotels.model.Reserved;
import com.softserve.hotels.service.ApartmentService;
import com.softserve.hotels.service.ReservedService;
import com.softserve.hotels.service.UserService;

@Test
@WebAppConfiguration
@ContextConfiguration(classes = { AppConfig.class})
@TestPropertySource(locations = "classpath:testdb.properties")
public class ReservedServiceTest extends AbstractTransactionalTestNGSpringContextTests{

    @Autowired
    ReservedService reservedService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    ApartmentService apartmentService;
    
    @BeforeClass
    void setUp() {
        
    }

    @Transactional
    public void testReservedFilter() {
         ActiveOrders activeOrders = new ActiveOrders();
         activeOrders.setCurrentPage(1);
         activeOrders.setPageSize(5);
         activeOrders.setName("");
         activeOrders.setTenant(userService.findUserByEmail("user@gmail.com"));
         activeOrders.setActionStatus(ActionStatus.ALL);
         Assert.assertEquals(reservedService.filterReserved(activeOrders).size(), 5);
         Assert.assertEquals(activeOrders.getEntityCount(), 15);
         
         activeOrders.setActionStatus(ActionStatus.WAITING_PAYMENT);
         Assert.assertEquals(reservedService.filterReserved(activeOrders).size(), 1);
    }
    
    @Transactional
    public void testReservedFilterRenter(){
        RenterOrdersDto renterFilters = new RenterOrdersDto();
        renterFilters.setCurrentPage(1);
        renterFilters.setPageSize(5);
        renterFilters.setApartment(apartmentService.findById(1));
        renterFilters.setRenter(userService.findUserByEmail("renter@gmail.com"));
        renterFilters.setActionStatus(ActionStatus.WAITING_CONFIRMATION);
        Assert.assertEquals(reservedService.filterRenterReservations(renterFilters).size(), 2);
        
        renterFilters.setActionStatus(ActionStatus.WAITING_PAYMENT);
        Assert.assertEquals(reservedService.filterRenterReservations(renterFilters).size(), 0);
        
        renterFilters.setApartment(apartmentService.findById(2));
        renterFilters.setActionStatus(ActionStatus.WAITING_PAYMENT);
        Assert.assertEquals(reservedService.filterRenterReservations(renterFilters).size(), 1);
    }
    
    @Transactional
    public void deleteAllUnpayedForUser(){
        ActiveOrders activeOrders = new ActiveOrders();
        activeOrders.setCurrentPage(1);
        activeOrders.setPageSize(5);
        activeOrders.setName("");
        activeOrders.setTenant(userService.findUserByEmail("user@gmail.com"));
        activeOrders.setActionStatus(ActionStatus.WAITING_PAYMENT);
        int notPayed = reservedService.filterReserved(activeOrders).size();
        
        reservedService.deleteAllUnpayedForUser(userService.findUserByEmail("user@gmail.com"));
        Assert.assertEquals(reservedService.filterReserved(activeOrders).size(), notPayed - 1);
        
    }
    
    @Transactional
    public void aproveReservation(){
        RenterOrdersDto renterFilters = new RenterOrdersDto();
        renterFilters.setCurrentPage(1);
        renterFilters.setPageSize(5);
        renterFilters.setApartment(apartmentService.findById(1));
        renterFilters.setRenter(userService.findUserByEmail("renter@gmail.com"));
        renterFilters.setActionStatus(ActionStatus.APROVED);
        reservedService.filterRenterReservations(renterFilters);
        int notConfirmed = renterFilters.getEntityCount();
        
        reservedService.confirmReservation(reservedService.findById(7));
        reservedService.filterRenterReservations(renterFilters);
        Assert.assertEquals(renterFilters.getEntityCount(), notConfirmed + 1);
        
    }
    
    @Transactional
    public void declineReservation(){
        RenterOrdersDto renterFilters = new RenterOrdersDto();
        renterFilters.setCurrentPage(1);
        renterFilters.setPageSize(5);
        renterFilters.setApartment(apartmentService.findById(1));
        renterFilters.setRenter(userService.findUserByEmail("renter@gmail.com"));
        renterFilters.setActionStatus(ActionStatus.DECLINED_RENTER);
        reservedService.filterRenterReservations(renterFilters);
        int notConfirmed = renterFilters.getEntityCount();
          
        reservedService.declineReservationByRenter(reservedService.findById(7), "");
        reservedService.filterRenterReservations(renterFilters);
        Assert.assertEquals(renterFilters.getEntityCount(), notConfirmed + 1);
        
    }
    
    @Transactional
     public void bookApartment(){
        Reserved reserved = new Reserved();
        reserved.setApartment(apartmentService.findById(1));
        reserved.setDateStartReservation(new LocalDate("2016-03-21"));
        reserved.setDateEndReservation(new LocalDate("2016-03-23"));
        reserved.setTenant(userService.findUserByEmail("user@gmail.com"));
        Assert.assertEquals(reservedService.isReservedForTime(reserved), false);
        
        
        reserved.setStatus(ActionStatus.WAITING_PAYMENT);
        reservedService.create(reserved);
        
        Assert.assertEquals(reservedService.isReservedForTime(reserved), true);
        
    }

}
