package com.softserve.hotels.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.softserve.hotels.configuration.AppConfig;
import com.softserve.hotels.dto.ApartmentInfoDto;
import com.softserve.hotels.dto.ApartmentOrders;
import com.softserve.hotels.model.ApartmentStatus;
import com.softserve.hotels.service.ApartmentService;
import com.softserve.hotels.service.ReservedService;
import com.softserve.hotels.service.UserService;

@Test
@WebAppConfiguration
@ContextConfiguration(classes = { AppConfig.class})
@TestPropertySource(locations = "classpath:testdb.properties")
public class ApartmentServiceTest extends AbstractTransactionalTestNGSpringContextTests{
    
    @Autowired
    ReservedService reservedService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    ApartmentService apartmentService;
    
    @BeforeClass
    void setUp() {
        
    }
    
    @Transactional
    public void testApartmentFilter() {
        ApartmentOrders apartmentOrders = new ApartmentOrders();
        apartmentOrders.setCurrentPage(1);
        
        apartmentOrders.setIdCity("ChIJV5oQCXzdOkcR4ngjARfFI0I");
        
        apartmentOrders.setMaxCountGuests(5);
        apartmentService.filterApartment(apartmentOrders);
        Assert.assertEquals(apartmentOrders.getEntityCount(), 2);
        
        apartmentOrders = new ApartmentOrders();
        
        apartmentOrders.setStartPrice(50.f);
        apartmentOrders.setEndPrice(150.f);
        
        apartmentOrders.setStartRaiting(1.f);
        apartmentOrders.setEndRaiting(4.f);
        
        apartmentOrders.setPaymentMethod(2);
        
        apartmentService.filterApartment(apartmentOrders);
        Assert.assertEquals(apartmentOrders.getEntityCount(), 1);
        
        List<Integer> convenience = new ArrayList<Integer>(Arrays.asList(1,3));
        apartmentOrders.setConveniences(convenience);
        
        Assert.assertEquals(apartmentService.filterApartment(apartmentOrders).size(), 1);
        Assert.assertEquals(apartmentOrders.getEntityCount(), 1);
    }
    
    @Transactional
    public void publishApartmentTest(){
        ApartmentInfoDto apartmentInfoDto = new ApartmentInfoDto();
        apartmentInfoDto.setPublished(false);
        apartmentInfoDto.setApartmentStatus(ApartmentStatus.ENABLED);
        apartmentInfoDto.setRenter(userService.findUserByEmail("renter@gmail.com"));
        int unpublished = apartmentService.findApartmentsForRenter(apartmentInfoDto).size();
        
        apartmentService.unPublishApartment(apartmentService.findById(1));
        Assert.assertEquals(apartmentService.findApartmentsForRenter(apartmentInfoDto).size(), unpublished+1);
        
        apartmentInfoDto.setPublished(true);
        int published = apartmentService.findApartmentsForRenter(apartmentInfoDto).size();
        apartmentService.publishApartment(apartmentService.findById(1));
        Assert.assertEquals(apartmentService.findApartmentsForRenter(apartmentInfoDto).size(), published+1);        
    }

}
