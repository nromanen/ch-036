package com.softserve.hotels.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.softserve.hotels.dto.QueryInfoDto;
import com.softserve.hotels.model.Apartment;
import com.softserve.hotels.model.Reserved;
import com.softserve.hotels.model.Role;
import com.softserve.hotels.model.User;
import com.softserve.hotels.model.VerificationToken;

/**
 * @author Rogulya Volodymyr
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDaoImpl<User> implements UserDao {

    public static final Logger LOG = LogManager.getLogger(UserDaoImpl.class);

    @Override
    public User findUserByNickname(String nickname) {
        try {
    	TypedQuery<User> query = getEntityManager().createNamedQuery(User.NQ_FIND_USER_BY_NICKNAME, User.class);
            return query.setParameter("nick", nickname).getSingleResult();
        } catch (NoResultException e) {
            LOG.info("Such user is not found");
            LOG.debug(e);
            return null;
        }
    }

    @Override
    public List<User> findUserByRole(Role role) {
        TypedQuery<User> query = getEntityManager().createNamedQuery(User.NQ_FIND_ALL_BY_ROLE, User.class);
        return query.setParameter("role", role).getResultList();
    }

    @Override
    public User findUserByEmail(String email) {
        try {
    	TypedQuery<User> query = getEntityManager().createNamedQuery(User.NQ_FIND_USER_BY_EMAIL, User.class);
        return query.setParameter("email", email).getSingleResult();
        } catch (NoResultException e) {
            LOG.info("Such email is not found");
            LOG.debug(e);
            return null;
        }
    }

    @Override
    public List<User> findUserLikeEmailAndByRolePageable(QueryInfoDto userListPageInfo) {
        int currentPage = userListPageInfo.getCurrentPage();
        int pageSize = userListPageInfo.getPageSize();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> root = query.from(User.class);
        List<Predicate> predicates = new ArrayList<>();
        if (userListPageInfo.getEmail() != null) {
            String emailPattern = "%" + userListPageInfo.getEmail() + "%";
            Predicate emailPredicate = criteriaBuilder
                    .like(criteriaBuilder.lower(root.<String> get("email")),
                            emailPattern.toLowerCase());
            predicates.add(emailPredicate);
        }
        if (userListPageInfo.getRole() != null) {
            Predicate rolePredicate = criteriaBuilder.equal(root.<Role>get("role"), userListPageInfo.getRole());
            predicates.add(rolePredicate);
        }
        if (predicates.size() > 0) {
            query.where(predicates.toArray(new Predicate[predicates.size()]));
        }
        applyOrdering(criteriaBuilder, query, root, userListPageInfo);
        List<User> result = findByRange(query, (currentPage - 1) * pageSize, pageSize);
        userListPageInfo.setEntityCount(getEntityCount(criteriaBuilder, query, root).intValue());
        return result;
    }

    @Override
    public List<User> findAllUsersPageable(QueryInfoDto userListPageInfo) {
        int currentPage = userListPageInfo.getCurrentPage();
        int pageSize = userListPageInfo.getPageSize();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> root = query.from(User.class);
        applyOrdering(criteriaBuilder, query, root, userListPageInfo);
        List<User> resultList = findByRange(query, (currentPage - 1) * pageSize, pageSize);
        userListPageInfo.setEntityCount(getEntityCount(criteriaBuilder, query, root).intValue());
        return resultList;
    }
    
    @Override
    public List<User> findUserRenterUnapproved(Role role) {
        try {
            //"SELECT users.id, verification_tokens.token FROM User users, VerificationToken verification_tokens WHERE users.id = verification_tokens.user AND verification_tokens.token != '' AND users.role = 1 AND users.enabled = false");
            //Query query = getEntityManager().createQuery("FROM User WHERE role = :role AND enabled = false ");
            Query query = getEntityManager().createQuery("FROM VerificationToken verification_tokens WHERE verification_tokens.token != '' AND verification_tokens.user.role = 1 AND verification_tokens.user.enabled = false");
            //TypedQuery<VerificationToken> query = getEntityManager().createNamedQuery(VerificationToken.NQ_FIND_USER_RENTER_UNAPPROVED, VerificationToken.class);
            //return query.setParameter("role", role).getResultList();
            return query.getResultList();
        } catch (NoResultException e) {
            LOG.info("Such users is not found");
            LOG.debug(e);
            return null;
        }
    }

}
