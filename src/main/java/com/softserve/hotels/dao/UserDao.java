package com.softserve.hotels.dao;

import java.util.List;

import com.softserve.hotels.dto.QueryInfoDto;
import com.softserve.hotels.model.Role;
import com.softserve.hotels.model.User;
import com.softserve.hotels.model.VerificationToken;

public interface UserDao extends AbstractDao<User> {

    User findUserByNickname(String nickname);

    User findUserByEmail(String email);

    List<User> findUserByRole(Role role);

    List<User> findUserLikeEmailAndByRolePageable(QueryInfoDto userPageInfo);
    
    List<User> findAllUsersPageable(QueryInfoDto userListPageInfo);
    
    List<User> findUserRenterUnapproved(Role role);
}