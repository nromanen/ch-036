package com.softserve.hotels.dao;

import java.util.List;

import com.softserve.hotels.dto.ApartmentInfoDto;
import com.softserve.hotels.dto.ApartmentOrders;
import com.softserve.hotels.dto.ModeratorInfoDto;
import com.softserve.hotels.dto.QueryInfoDto;
import com.softserve.hotels.model.Apartment;
import com.softserve.hotels.model.User;

public interface ApartmentDAO extends AbstractDao<Apartment> {

    List<Apartment> findAllInDescOrder();

    void updateApartmentInfo(Apartment apartment);

    List<Apartment> findAllEnabledForRenter(User user);

    List<Apartment> findAllDisabledForRenter(User user);

    List<Apartment> findAllUnpublishedForRenter(User user);

    List<Apartment> findForModerator(ModeratorInfoDto moderatorDto);

    List<Apartment> findAllEnabledForModerator(User user);

    List<Apartment> findAllDisabledForModerator(User user);

    List<Apartment> findAllFree();

    List<Apartment> findAllMy(User user);

    List<Apartment> findAllOther(User user);

    List<Apartment> findApartmentsForRenter(ApartmentInfoDto apartmentPageInfo);

    List<Apartment> findAllEnabledForRenterPageable(User user, QueryInfoDto apartmentPageInfo);

    List<Apartment> filterApartments(ApartmentOrders apartmentOrders);

}
