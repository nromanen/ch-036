package com.softserve.hotels.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Repository;

import com.softserve.hotels.dto.ApartmentInfoDto;
import com.softserve.hotels.dto.ApartmentOrders;
import com.softserve.hotels.dto.ModeratorInfoDto;
import com.softserve.hotels.dto.QueryInfoDto;
import com.softserve.hotels.model.Apartment;
import com.softserve.hotels.model.ApartmentConveniences;
import com.softserve.hotels.model.ApartmentConveniences_;
import com.softserve.hotels.model.ApartmentPayment;
import com.softserve.hotels.model.ApartmentPayment_;
import com.softserve.hotels.model.ApartmentStatus;
import com.softserve.hotels.model.Apartment_;
import com.softserve.hotels.model.Reserved;
import com.softserve.hotels.model.Reserved_;
import com.softserve.hotels.model.User;

@Repository("apartmentDao")
public class ApartmentDAOImpl extends AbstractDaoImpl<Apartment>implements ApartmentDAO {

    public static final Logger LOG = LogManager.getLogger(UserDaoImpl.class);

    @Override
    public List<Apartment> findAllInDescOrder() {
        try {
            return getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_ORDER_BY_ID_DESC, Apartment.class)
                    .getResultList();
        } catch (NoResultException e) {
            LOG.info(e);
            return new ArrayList<>();
        }
    }

    @Override
    public void updateApartmentInfo(Apartment apartment) {
        Query query = getEntityManager().createNamedQuery(Apartment.NQ_UPDATE_APARTMENT_INFO);
        query.setParameter("name", apartment.getName());
        query.setParameter("description", apartment.getDescription());
        query.setParameter("address", apartment.getAddress());
        query.setParameter("city", apartment.getCity());
        query.setParameter("id", apartment.getId());
        query.executeUpdate();
    }

    @Override
    public List<Apartment> findAllEnabledForRenter(User user) {
        TypedQuery<Apartment> query = getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_ENABLED_FOR_USER,
                Apartment.class);
        query.setParameter("renter", user);
        query.setParameter("status", ApartmentStatus.ENABLED);
        return query.getResultList();
    }

    @Override
    public List<Apartment> findAllDisabledForRenter(User user) {
        TypedQuery<Apartment> query = getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_DISABLED_FOR_USER,
                Apartment.class);
        query.setParameter("renter", user);
        query.setParameter("status", ApartmentStatus.ENABLED);
        return query.getResultList();
    }

    @Override
    public List<Apartment> findAllUnpublishedForRenter(User user) {
        TypedQuery<Apartment> query = getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_UNPUBLISHED_FOR_USER,
                Apartment.class);
        query.setParameter("renter", user);
        query.setParameter("status", ApartmentStatus.ENABLED);
        return query.getResultList();
    }

    @Override
    public List<Apartment> findForModerator(ModeratorInfoDto moderatorDto) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Apartment> query = criteriaBuilder.createQuery(Apartment.class);
        Root<Apartment> entityRoot = query.from(Apartment.class);
        query.select(entityRoot);

        ArrayList<Predicate> predicates = new ArrayList<>();
        if (moderatorDto.getModerator() != null) {
            predicates.add(criteriaBuilder.equal(entityRoot.get(Apartment_.moderator), moderatorDto.getModerator()));
        } else {
            predicates.add(criteriaBuilder.isNull(entityRoot.get(Apartment_.moderator)));
        }

        if (moderatorDto.getStatus() != null) {
            predicates.add(criteriaBuilder.equal(entityRoot.get(Apartment_.status), moderatorDto.getStatus()));
        }

        if (moderatorDto.getPublish() != null) {
            predicates.add(criteriaBuilder.equal(entityRoot.get(Apartment_.published), moderatorDto.getPublish()));
        }

        query.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

        List<Apartment> resultList = findByRange(query,
                (moderatorDto.getCurrentPage() - 1) * moderatorDto.getPageSize(), moderatorDto.getPageSize());
        moderatorDto.setEntityCount(getEntityCount(criteriaBuilder, query, entityRoot).intValue());
        moderatorDto.setCurrentPage(moderatorDto.getCurrentPage());
        return resultList;
    }

    @Override
    public List<Apartment> findAllEnabledForModerator(User user) {
        TypedQuery<Apartment> query = getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_STATUS_FOR_MODERATOR,
                Apartment.class);
        query.setParameter("moderator", user);
        query.setParameter("status", ApartmentStatus.ENABLED);
        return query.getResultList();
    }

    @Override
    public List<Apartment> findAllDisabledForModerator(User user) {
        TypedQuery<Apartment> query = getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_STATUS_FOR_MODERATOR,
                Apartment.class);
        query.setParameter("moderator", user);
        query.setParameter("status", ApartmentStatus.DISABLED_BY_MODERATOR);
        return query.getResultList();
    }

    @Override
    public List<Apartment> findAllFree() {
        TypedQuery<Apartment> query = getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_FREE, Apartment.class);
        return query.getResultList();
    }

    @Override
    public List<Apartment> findAllMy(User user) {
        TypedQuery<Apartment> query = getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_MY, Apartment.class);
        query.setParameter("id", user.getId());
        return query.getResultList();
    }

    @Override
    public List<Apartment> findAllOther(User user) {
        TypedQuery<Apartment> query = getEntityManager().createNamedQuery(Apartment.NQ_FIND_ALL_OTHER, Apartment.class);
        query.setParameter("id", user.getId());
        return query.getResultList();
    }

    @Override
    public List<Apartment> findApartmentsForRenter(ApartmentInfoDto apartmentPageInfo) {
        int currentPage = apartmentPageInfo.getCurrentPage();
        int pageSize = apartmentPageInfo.getPageSize();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Apartment> query = criteriaBuilder.createQuery(Apartment.class);
        Root<Apartment> root = query.from(Apartment.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get(Apartment_.renter), apartmentPageInfo.getRenter()));

        if (apartmentPageInfo.getApartmentStatus() != null) {
            predicates.add(criteriaBuilder.equal(root.get(Apartment_.status), apartmentPageInfo.getApartmentStatus()));
        }

        if (apartmentPageInfo.getPublished() != null) {
            predicates.add(criteriaBuilder.equal(root.get(Apartment_.published), apartmentPageInfo.getPublished()));
        }
        query.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
        List<Apartment> resultList = findByRange(query, (currentPage - 1) * pageSize, pageSize);
        apartmentPageInfo.setEntityCount(getEntityCount(criteriaBuilder, query, root).intValue());
        return resultList;
    }

    @Override
    public List<Apartment> findAllEnabledForRenterPageable(User user, QueryInfoDto apartmentInfoDto) {
        int currentPage = apartmentInfoDto.getCurrentPage();
        int pageSize = apartmentInfoDto.getPageSize();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Apartment> query = criteriaBuilder.createQuery(Apartment.class);
        Root<Apartment> root = query.from(Apartment.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get(Apartment_.renter), user));
        predicates.add(criteriaBuilder.equal(root.get(Apartment_.status), ApartmentStatus.ENABLED));
        predicates.add(criteriaBuilder.equal(root.get(Apartment_.published), true));
        query.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
        List<Apartment> resultList = findByRange(query, (currentPage - 1) * pageSize, pageSize);
        apartmentInfoDto.setEntityCount(getEntityCount(criteriaBuilder, query, root).intValue());
        return resultList;
    }

    @Override
    public List<Apartment> filterApartments(ApartmentOrders orderFilter) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Apartment> query = criteriaBuilder.createQuery(Apartment.class);
        Root<Apartment> apartmentRoot = query.from(Apartment.class);
        query.select(apartmentRoot);
        ArrayList<Predicate> predicates = new ArrayList<>();

        if (orderFilter.getIdCity() != null && orderFilter.getIdCity() != "") {
            predicates.add(criteriaBuilder.equal(apartmentRoot.get(Apartment_.idCity), orderFilter.getIdCity()));
        }

        if (orderFilter.getStartDate() != null && orderFilter.getEndDate() != null) {
            predicates.add(criteriaBuilder.in(apartmentRoot)
                    .value(reservedSubquery(criteriaBuilder, orderFilter, query)).not());
        }

        if (orderFilter.getStartRaiting() != null && orderFilter.getEndRaiting() != null) {
            predicates.add(criteriaBuilder.between(apartmentRoot.get(Apartment_.raiting), orderFilter.getStartRaiting(),
                    orderFilter.getEndRaiting()));
        }

        if (orderFilter.getStartPrice() != null && orderFilter.getEndPrice() != null) {
            predicates.add(criteriaBuilder.between(apartmentRoot.get(Apartment_.price), orderFilter.getStartPrice(),
                    orderFilter.getEndPrice()));
        }

        if (orderFilter.getMaxCountGuests() != null && orderFilter.getMaxCountGuests() != 0) {
            predicates.add(criteriaBuilder.equal(apartmentRoot.get(Apartment_.maxCountGuests),
                    orderFilter.getMaxCountGuests()));
        }

        if (orderFilter.getName() != null && orderFilter.getName() != "") {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(apartmentRoot.<String> get(Apartment_.name)),
                    "%" + orderFilter.getName().toLowerCase() + "%"));
        }

        if (orderFilter.getPaymentMethod() != null) {
            predicates.add(criteriaBuilder.exists(paymentSubquery(criteriaBuilder, orderFilter, apartmentRoot, query)));
        }

        if (orderFilter.getConveniences() != null) {
            predicates.add(
                    criteriaBuilder.exists(convenienceSubquery(criteriaBuilder, orderFilter, apartmentRoot, query)));
        }

        predicates.add(criteriaBuilder.equal(apartmentRoot.get(Apartment_.published), true));
        predicates.add(criteriaBuilder.equal(apartmentRoot.get(Apartment_.status), ApartmentStatus.ENABLED));

        CriteriaQuery<Apartment> sortQuery = SotrBy(criteriaBuilder, orderFilter, apartmentRoot, query);
        sortQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
        sortQuery.distinct(true);

        List<Apartment> resultList = findByRange(sortQuery,
                (orderFilter.getCurrentPage() - 1) * orderFilter.getPageSize(), orderFilter.getPageSize());
        orderFilter.setEntityCount(getEntityCount(criteriaBuilder, sortQuery, apartmentRoot).intValue());
        orderFilter.setCurrentPage(orderFilter.getCurrentPage());
        return resultList;
    }

    private Subquery<Apartment> reservedSubquery(CriteriaBuilder criteriaBuilder, ApartmentOrders orderFilter,
            CriteriaQuery<Apartment> query) {
        Subquery<Apartment> subquery = query.subquery(Apartment.class);
        Root<Reserved> reserved = subquery.from(Reserved.class);
        subquery.select(reserved.get(Reserved_.apartment));

        Path<LocalDate> dsr = reserved.get(Reserved_.dateStartReservation);
        Path<LocalDate> der = reserved.get(Reserved_.dateEndReservation);
        LocalDate startDate = orderFilter.getStartDate();
        LocalDate endDate = orderFilter.getEndDate();

        Predicate reservedPredicate = criteriaBuilder.or(
                criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(dsr, startDate),
                        criteriaBuilder.greaterThanOrEqualTo(der, startDate),
                        criteriaBuilder.lessThanOrEqualTo(der, endDate)),
                criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(dsr, startDate),
                        criteriaBuilder.lessThanOrEqualTo(der, endDate)),
                criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(dsr, startDate),
                        criteriaBuilder.lessThanOrEqualTo(dsr, endDate),
                        criteriaBuilder.greaterThanOrEqualTo(der, endDate)),
                criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(dsr, startDate),
                        criteriaBuilder.greaterThanOrEqualTo(der, endDate)));
        subquery.where(criteriaBuilder.and(reservedPredicate));
        return subquery;
    }

    private Subquery<ApartmentPayment> paymentSubquery(CriteriaBuilder criteriaBuilder, ApartmentOrders orderFilter,
            Root<Apartment> apartmentRoot, CriteriaQuery<Apartment> query) {

        Subquery<ApartmentPayment> subquery = query.subquery(ApartmentPayment.class);
        Root<ApartmentPayment> apartmentPaymentRoot = subquery.from(ApartmentPayment.class);
        subquery.select(apartmentPaymentRoot);

        Predicate predicate = criteriaBuilder.and(
                criteriaBuilder.equal(apartmentRoot, apartmentPaymentRoot.get(ApartmentPayment_.apartment)),
                criteriaBuilder.equal(apartmentPaymentRoot.get(ApartmentPayment_.exists), true), criteriaBuilder.equal(
                        apartmentPaymentRoot.get(ApartmentPayment_.paymentMethod), orderFilter.getPaymentMethod()));
        subquery.where(criteriaBuilder.and(predicate));
        return subquery;
    }

    private Subquery<ApartmentConveniences> convenienceSubquery(CriteriaBuilder criteriaBuilder,
            ApartmentOrders orderFilter, Root<Apartment> apartmentRoot, CriteriaQuery<Apartment> query) {

        Subquery<ApartmentConveniences> subquery = query.subquery(ApartmentConveniences.class);
        Root<ApartmentConveniences> apartmentConveniencesRoot = subquery.from(ApartmentConveniences.class);
        subquery.select(apartmentConveniencesRoot);

        for (Integer convenience : orderFilter.getConveniences()) {
            Predicate predicate = criteriaBuilder.and(
                    criteriaBuilder.equal(apartmentRoot,
                            apartmentConveniencesRoot.get(ApartmentConveniences_.apartment)),
                    criteriaBuilder.equal(apartmentConveniencesRoot.get(ApartmentConveniences_.exists), true),
                    criteriaBuilder.equal(apartmentConveniencesRoot.get(ApartmentConveniences_.convenience),
                            convenience));
            subquery.where(criteriaBuilder.and(predicate));
        }
        return subquery;
    }

    private CriteriaQuery<Apartment> SotrBy(CriteriaBuilder criteriaBuilder, ApartmentOrders orderFilter,
            Root<Apartment> apartmentRoot, CriteriaQuery<Apartment> query) {

        if (orderFilter.getSortBy() != null && orderFilter.getSortBy().equals("sortByPriceASC")) {
            query.orderBy(criteriaBuilder.asc(apartmentRoot.get(Apartment_.price)));
        } else if (orderFilter.getSortBy() != null && orderFilter.getSortBy().equals("sortByRatingASC")) {
            query.orderBy(criteriaBuilder.asc(apartmentRoot.get(Apartment_.raiting)));
        } else if (orderFilter.getSortBy() != null && orderFilter.getSortBy().equals("sortByCityDESC")) {
            query.orderBy(criteriaBuilder.desc(apartmentRoot.get(Apartment_.city)));
        } else if (orderFilter.getSortBy() != null && orderFilter.getSortBy().equals("sortByPriceDESC")) {
            query.orderBy(criteriaBuilder.desc(apartmentRoot.get(Apartment_.price)));
        } else if (orderFilter.getSortBy() != null && orderFilter.getSortBy().equals("sortByRatingDESC")) {
            query.orderBy(criteriaBuilder.desc(apartmentRoot.get(Apartment_.raiting)));
        } else {
            query.orderBy(criteriaBuilder.asc(apartmentRoot.<String> get(Apartment_.city)));
        }

        return query;
    }

}
