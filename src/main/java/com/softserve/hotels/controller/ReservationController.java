package com.softserve.hotels.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.softserve.hotels.dto.CanceledReservation;
import com.softserve.hotels.dto.Info;
import com.softserve.hotels.dto.QueryInfoDto;
import com.softserve.hotels.dto.RenterOrdersDto;
import com.softserve.hotels.model.ActionStatus;
import com.softserve.hotels.model.Apartment;
import com.softserve.hotels.model.Reserved;
import com.softserve.hotels.service.ApartmentService;
import com.softserve.hotels.service.ReservedService;

@Controller
@RequestMapping(value = "/renter")
public class ReservationController {

    @Autowired
    private ReservedService reservedService;

    @Autowired
    private ApartmentService apartmentService;

    @RequestMapping(value = "/removeReservationByRenter", method = RequestMethod.POST)
    public String removeReservationByRenter(
            @ModelAttribute("renterOrdersDto") RenterOrdersDto renterOrdersDto,
            Model model) {
        Reserved reserved = this.reservedService.findById(renterOrdersDto.getIdReservation());
        switch (reserved.getStatus()) {
        case APROVED:
            this.reservedService.declineReservationByRenter(reserved, renterOrdersDto.getComment());
             break;
        case WAITING_PAYMENT:
            this.reservedService.declineReservationByRenterFake(reserved, renterOrdersDto.getComment());
            break;
        case WAITING_CONFIRMATION:
            this.reservedService.declineReservationByRenterBadDeal(reserved, renterOrdersDto.getComment());
            break;
        default:
            this.reservedService.declineReservationByRenter(reserved, renterOrdersDto.getComment());
            break;
        }

        return renterOrdersSet(model, reserved.getApartment(), renterOrdersDto);
    }

    @RequestMapping(value = "/confirmReservation/{id}", method = RequestMethod.GET)
    public String confirmReservation(@PathVariable("id") int id, Model model) {
        Reserved reserved = reservedService.findById(id);
        reservedService.confirmReservation(reserved);
        RenterOrdersDto renterOrdersDto = new RenterOrdersDto();
        renterOrdersDto.setActionStatus(ActionStatus.WAITING_CONFIRMATION);
        return renterOrdersSet(model, reserved.getApartment(), renterOrdersDto);        
    }

    @RequestMapping(value = "/reservationDetail/{id}", method = RequestMethod.GET)
    public String reservationDetail(@PathVariable("id") int idReseration, Model model) {
        Reserved reservation = this.reservedService.findById(idReseration);
        model.addAttribute("reservation", reservation);
        model.addAttribute("apartment", reservation.getApartment());
        return "reservationDetail";
    }

    @RequestMapping(value = "/backToHistory/{id}", method = RequestMethod.GET)
    public String backToHistory(@PathVariable("id") int idReseration, Model model) {
        Reserved reservation = this.reservedService.findById(idReseration);
        model.addAttribute("apartment", reservation.getApartment());
        model.addAttribute("pastReserved", this.reservedService.getPastReserved(reservation.getApartment()));
        return "historyReservations";
    }



    @RequestMapping(value = "/renterOrders/{id}", method = RequestMethod.GET)
    public String renterOrders(@PathVariable("id") int idApartment, Model model,
            @RequestParam(value = "actionStatus", defaultValue = "") ActionStatus actionStatus,
            @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
            @RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage) {
        Apartment apartment = this.apartmentService.findById(idApartment);
        
        RenterOrdersDto renterOrdersDto = new RenterOrdersDto();
        renterOrdersDto.setActionStatus(actionStatus);
        renterOrdersDto.setCurrentPage(currentPage);
        renterOrdersDto.setPageSize(pageSize);

        return renterOrdersSet(model, apartment, renterOrdersDto);
    }
    
  
    private String renterOrdersSet(Model model, Apartment apartment, RenterOrdersDto renterOrdersDto) {
        
        renterOrdersDto.setApartment(apartment);
        model.addAttribute("apartment", apartment);
        model.addAttribute("renterOrders", this.reservedService.filterRenterReservations(renterOrdersDto));
        model.addAttribute("renterOrdersDto", renterOrdersDto);      
        return "renterReservations";
    }

}
