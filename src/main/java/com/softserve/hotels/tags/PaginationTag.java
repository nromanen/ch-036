package com.softserve.hotels.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class PaginationTag extends SimpleTagSupport {

    private int currentPage;
    private int entityCount;
    private int pageSize = 3;

    private int startPage;
    private int endPage;
    private int totalPages;
    private int PAGES_RANGE = 5;

    @Override
    public void doTag() throws JspException, IOException {
        paginationCalculates();
        JspWriter out = getJspContext().getOut();

        // pages links drawing
        if (endPage > 1) {
            
            out.println("<ul class=\"pagination\">");
            // draw back link
            out.println(backLinkCreate(startPage > PAGES_RANGE));

            for (int i = startPage; i <= endPage; i++) {
                out.println(pageLinkCreate(i));
            }

            // draw forward link, if needs
            out.println(forwardLinkCreate(endPage < totalPages));
            out.println("</ul>");
            
        }

    }

    private String backLinkCreate(boolean active) {
        StringBuilder link;
        if(active) {
             link = new StringBuilder("<li>");
            link.append("<a href=\"#\" btnIndex=\"" + (startPage - 1) + "\" class='submitLink'>");
        }
        else {
             link = new StringBuilder("<li class=\"disabled\">");
            link.append("<a href=\"#\">");
        }
        link.append("&laquo;");
        link.append("</a></li>");
        return link.toString();
    }

    private String forwardLinkCreate(boolean active) {
        StringBuilder link;
        if(active) {
            link = new StringBuilder("<li>");
           link.append("<a href=\"#\" btnIndex=\"" + (endPage+ 1) + "\" class='submitLink'>");
       }
       else {
            link = new StringBuilder("<li class=\"disabled\">");
           link.append("<a href=\"#\">");
       }
        link.append("&raquo;");
        link.append("</a></li>");
        return link.toString();
    }

    private String pageLinkCreate(int number) {
        StringBuilder link = number == currentPage ? new StringBuilder("<li class=\"active\">")
                : new StringBuilder("<li>");
        link.append("<a href=\"#\" btnIndex=\"" + number + "\" class=\"submitLink\">");
        link.append(number);
        link.append("</a></li>");
        return link.toString();
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setEntityCount(int entityCount) {
        this.entityCount = entityCount;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    private void paginationCalculates() {
        
        Long pageCount = (long) Math.ceil(((double) entityCount) / pageSize);
        totalPages = pageCount.intValue();

        startPage = (currentPage % PAGES_RANGE == 0) ? currentPage - (PAGES_RANGE - 1)
                : ((currentPage / PAGES_RANGE) * PAGES_RANGE + 1);

        endPage = (startPage + PAGES_RANGE) - 1;

        if (endPage >= totalPages) {
            endPage = totalPages;
        }
    }
}
