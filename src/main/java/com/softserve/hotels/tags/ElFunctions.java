package com.softserve.hotels.tags;

import com.softserve.hotels.model.ActionStatus;

public class ElFunctions {
    
    public static Boolean isActive( ActionStatus status)
    {
        if(status == ActionStatus.ALL_ACTIVE || status == ActionStatus.APROVED 
                || status == ActionStatus.WAITING_CONFIRMATION){
            return true;
        }
        return false;
    }
    
    public static Boolean isPayment( ActionStatus status)
    {
        if(status == ActionStatus.WAITING_PAYMENT){
            return true;
        }
        return false;
    }
}
