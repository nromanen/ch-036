package com.softserve.hotels.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.softserve.hotels.annotations.Loggable;
import com.softserve.hotels.dao.ReservedDao;
import com.softserve.hotels.model.ActionStatus;
import com.softserve.hotels.model.Reserved;

@Loggable
@Service
public class ScheduledServices {
    
    private Integer daysOffset;
    private String mailSubject;
    private Integer paymentLiveDuration;
    
    @Autowired
    private ConfigsService configsService;
    
    @Autowired
    private MailService mailService;

    @Autowired
    private ReservedDao reservedDao;
    
    @PostConstruct
    public void postConstruct() {
        daysOffset = Integer.parseInt(configsService.getConfigsByFeature("DaysOffset").getParameter());
        mailSubject = "We wait you in " + daysOffset + " days!";
        paymentLiveDuration = 72;
    }
    


    @Scheduled(cron = "0 0 0 * * ?")
    public void remindAboutReservation() {
        LocalDate dateForReminder = new LocalDate();
        dateForReminder = dateForReminder.plusDays(daysOffset);
        List<Reserved> reservationsToRemind = reservedDao.findActiveForDate(dateForReminder);
        for (Reserved item : reservationsToRemind) {
            String message = mailService.buildReminderMessage(item);
            mailService.sendMessage(item.getTenant(), mailSubject, message);
        }
    }
    
    @Scheduled(cron = "0 10 12 * * ?")
    public void remindUnpayed() {
        deleteUnpayedOvertime();
        ArrayList<Reserved> unpayedList = (ArrayList<Reserved>) reservedDao.findAllUnpayed();
        for (Reserved reserved : unpayedList) {
            mailService.sendMessage(reserved.getApartment().getRenter(), "Unpayed orders",
                    mailService.buildTenantUnpayedMessage(reserved));
        }
    }
    
    private void deleteUnpayedOvertime() {
        ArrayList<Reserved> unpayedList = (ArrayList<Reserved>) reservedDao.findAllUnpayed();
        for (Reserved reserved : unpayedList) {
            Hours hours = Hours.hoursBetween(reserved.getOrderingTime(), new LocalDateTime());
            if (hours.getHours() >= paymentLiveDuration) {
                reservedDao.delete(reserved);
                mailService.sendMessage(reserved.getApartment().getRenter(), "Delete order",
                        mailService.buildTenantDelUnpayedMessage(reserved));
            }
        }
    }
    
    @Scheduled(cron = "0 10 0 * * ?")
    public void changePastReservationsStatus() {
        List<Reserved> reservationsToChange = reservedDao.findPastReservedByStatuses(ActionStatus.APROVED);
        for (Reserved reserved : reservationsToChange) {
            reserved.setStatus(ActionStatus.AFTER_RESERVATION);
            reservedDao.update(reserved);
        }
    }
}
