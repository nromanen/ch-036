package com.softserve.hotels.service;

public interface PgDumper {
    
    void dump();
    
    void compress();
    
    void fullBackup();
    
}