package com.softserve.hotels.service;

import com.softserve.hotels.model.Reserved;
import com.softserve.hotels.model.User;

public interface MailService {
    void sendMessage(User user, String subject, String text);

    String buildRegisterMessage(User user, String token);
    
    String buildRegisterRenterMessage(User user);

    String buildUnbookMessage(User user);

    String buildReminderMessage(Reserved reserved);

    String buildTenantBookMessage(Reserved reserved);
    
    String buildUnbookMessageBecauseFake(User user);

    String buildConfirmReservMessage(User user);
    
    String buildCancelUserMessage(Reserved reserved);
    
    String buildRemidRenterMessage(Reserved reserved);
    
    String buildTenantUnpayedMessage(Reserved reserved);
    
    String buildTenantDelUnpayedMessage(Reserved reserved);
    
}
