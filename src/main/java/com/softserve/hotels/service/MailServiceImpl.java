package com.softserve.hotels.service;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.softserve.hotels.model.Reserved;
import com.softserve.hotels.model.User;

@Service
public class MailServiceImpl implements MailService {
	private static final Logger LOGGER = LogManager.getLogger(MailServiceImpl.class);

	@Autowired
	private ServletContext context;

	@Autowired
	private JavaMailSender mailSender;

	@Async()
	@Override
	public void sendMessage(User user, String subject, String text) {
		MimeMessage message = mailSender.createMimeMessage();
		boolean sended = false;
		MimeMessageHelper helper = new MimeMessageHelper(message);
		try {
			helper.setTo(user.getEmail());
			helper.setText(text, true);
			helper.setSubject(subject);
		} catch (MessagingException e) {
			LOGGER.error(e);
		}

		while (!sended) {
			try {
				synchronized (message) {
					mailSender.send(message);
					sended = true;
				}
				LOGGER.debug("Message " + message + "sended");
			} catch (MailException e) {

				LOGGER.error(e);
			}
		}
	}

	@Override
	public String buildRegisterMessage(User user, String token) {
		String confirmationUrl = "http://" + context.getVirtualServerName() + ":8080" + context.getContextPath()
				+ "/confirm?token=" + token;
		StringBuilder messageBuilder = new StringBuilder(
				"Hello, " + user.getNickname() + ". Welcome to our little project.");
		messageBuilder.append("To active your account please<a href=\'" + confirmationUrl + "\'> click here</a>. ");
		return messageBuilder.toString();
	}

	@Override
	public String buildUnbookMessage(User user) {
		StringBuilder messageBuilder = new StringBuilder(
				"Hello, " + user.getNickname() + ". Your order was rejected by renter.");
		messageBuilder.append("Sorry for that.");
		return messageBuilder.toString();
	}

	@Override
	public String buildUnbookMessageBecauseFake(User user) {
		StringBuilder messageBuilder = new StringBuilder(
				"Hello, " + user.getNickname() + ". Your order was rejected by renter, because you didn't come. ");
		messageBuilder.append("You are very irresponsible person.");
		return messageBuilder.toString();
	}

	@Override
	public String buildConfirmReservMessage(User user) {
		StringBuilder messageBuilder = new StringBuilder(
				"Hello, " + user.getNickname() + ". Your order was confirmed by renter. ");
		messageBuilder.append("Waiting for you.");
		return messageBuilder.toString();
	}

	@Override
	public String buildReminderMessage(Reserved reserved) {
		StringBuilder messageBuilder = new StringBuilder("Hello, " + reserved.getTenant().getNickname()
				+ ". We remind you, that in a 5 days you have to arrive in " + reserved.getApartment().getName());
		return messageBuilder.toString();
	}

	@Override
	public String buildCancelUserMessage(Reserved reserved) {
		StringBuilder messageBuilder = new StringBuilder("Hello, " + reserved.getApartment().getRenter().getNickname()
				+ ". User" + reserved.getTenant().getNickname() + ", who booked  " + reserved.getApartment().getName()
				+ ", canceled his order.");
		return messageBuilder.toString();
	}

	@Override
	public String buildRemidRenterMessage(Reserved reserved) {
		StringBuilder messageBuilder = new StringBuilder("Hello, " + reserved.getApartment().getRenter().getNickname()
				+ ". We remind you, that you have unreviewed orders. Please check them. Thank you and have a nice day.");
		return messageBuilder.toString();
	}

	@Override
	public String buildTenantBookMessage(Reserved reserved) {
		StringBuilder messageBuilder = new StringBuilder("Hello, " + reserved.getTenant().getNickname()
				+ ". You made order to apartment " + reserved.getApartment().getName()
				+ ", please wait for renter calling. Thank you and have a nice day.");
		return messageBuilder.toString();
	}

	@Override
	public String buildTenantUnpayedMessage(Reserved reserved) {
		StringBuilder messageBuilder = new StringBuilder("Hello, " + reserved.getTenant().getNickname()
				+ ". You made order to apartment " + reserved.getApartment().getName()
				+ ", but havn,t payed for it, please make payment. You order will be remove after 72 hours.");
		return messageBuilder.toString();
	}

	@Override
	public String buildTenantDelUnpayedMessage(Reserved reserved) {
		StringBuilder messageBuilder = new StringBuilder(
				"Hello, " + reserved.getTenant().getNickname() + ". Your order to apartment "
						+ reserved.getApartment().getName() + " deleted in spite of payment overtime!");
		return messageBuilder.toString();
	}

	@Override
	public String buildRegisterRenterMessage(User user) {
		StringBuilder messageBuilder = new StringBuilder(
				"Hello, " + user.getNickname() + ". Your profile waiting for approval by administrator.");
		messageBuilder.append("We contact you soon.");
		return messageBuilder.toString();
	}

}
