package com.softserve.hotels.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.softserve.hotels.utils.FileUtils;
import com.softserve.hotels.utils.ZipUtils;

public class PgWindowsDumper implements PgDumper {

    private static final Logger LOG = LogManager.getLogger(PgWindowsDumper.class);

    private String url;
    private String userName;
    private String password;
    private String postgresBinPath;
    private String host;
    private String port;
    private String database;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPostgresBinPath() {
        return postgresBinPath;
    }

    public void setPostgresBinPath(String postgresBinPath) {
        this.postgresBinPath = postgresBinPath;
    }

    @Override
    public void dump() {
        String data = url.split("//")[1];
        host = data.split(":")[0];
        data = data.split(":")[1];
        port = data.split("/")[0];
        database = data.split("/")[1];
        String pgDump = postgresBinPath + "\\pg_dump.exe";
        String backupPath = FileUtils.ROOT_PATH + File.separator + database
                + ".backup";
        String[] dump = { pgDump, "-Ft", "-p", port, "-U", userName, "-f", backupPath, database };
        try {
            ProcessBuilder pb = new ProcessBuilder(dump);
            Process process = pb.start();
            process.waitFor();

        } catch (IOException e) {
            LOG.error("can't make dump of DB");
            LOG.error(e);
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    @Override
    public void compress() {
        String folderToCompress = FileUtils.ROOT_PATH + File.separator;
        String compressedFile = FileUtils.ROOT_PATH + File.separator + "ExtractMeInServerRoot.zip";
        ZipUtils.compress(folderToCompress, compressedFile);
    }
    @Override
    public void fullBackup() {
        dump();
        compress();
    }

}
