package com.softserve.hotels.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.softserve.hotels.model.PhotoExtention;
import com.softserve.hotels.service.PhotoExtentionService;

@Component
public class AdminConfigValidator implements Validator {

    @Autowired
    private PhotoExtentionService photoExtentionService;
    
	@Override
	public boolean supports(Class<?> arg0) {
		return PhotoExtention.class.equals(arg0);
	}

	@Override
	public void validate(Object object, Errors errors) {
		PhotoExtention photoExtention = (PhotoExtention) object;
		if (this.photoExtentionService.findAll().contains(photoExtention)) {        
            errors.rejectValue("extention", "adminExtention.extensionsExists");
        }
        if (photoExtention.getExtention() == null || "".equals(photoExtention.getExtention())) {
            errors.rejectValue("extention", "apartmentDetail.emptyName");
        }
	}

}
