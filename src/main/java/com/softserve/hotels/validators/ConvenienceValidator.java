package com.softserve.hotels.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.softserve.hotels.model.Convenience;
import com.softserve.hotels.service.ConvenienceService;

@Component
public class ConvenienceValidator implements Validator{
	
	@Autowired
	ConvenienceService convenienceService; 

	@Override
	public boolean supports(Class<?> arg0) {
		return Convenience.class.equals(arg0);
	}

	@Override
	public void validate(Object object, Errors errors) {
		Convenience conveniences = (Convenience) object;
		if (this.convenienceService.findAll().contains(conveniences)) {        
            errors.rejectValue("name", "allConveniences.convenienceExists");
        }
        if (conveniences.getName() == null || "".equals(conveniences.getName())) {
            errors.rejectValue("name", "apartmentDetail.emptyName");
        }
		
	}

}
