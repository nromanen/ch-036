package com.softserve.hotels.dto;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.softserve.hotels.model.ActionStatus;
import com.softserve.hotels.model.User;

public class ActiveOrders{

    private User tenant;
    
    private int entityCount;
    private int currentPage;
    private int pageSize;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    private String name;
    
    private ActionStatus actionStatus;

    public void setTenant(User tenant) {
        this.tenant = tenant;
    }

    public User getTenant() {
        return tenant;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

  
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

 
    public int getCurrentPage() {
        return currentPage;
    }

  
    public void setEntityCount(int entityCount) {
        this.entityCount = entityCount;
    }

    public int getEntityCount() {
        return entityCount;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    
    public int getPageSize() {
        return pageSize;
    }
    
    public void setActionStatus(ActionStatus actionStatus) {
        
        this.actionStatus = (actionStatus == null)? ActionStatus.ALL: actionStatus;
    }
    
    public ActionStatus getActionStatus() {
        return actionStatus;
    }
        
}
