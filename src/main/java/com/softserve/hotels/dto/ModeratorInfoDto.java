package com.softserve.hotels.dto;

import com.softserve.hotels.model.ApartmentStatus;
import com.softserve.hotels.model.User;

public class ModeratorInfoDto {
    private User moderator;
    private ApartmentStatus status;
    private Boolean publish;
    
    private int entityCount;
    private Integer currentPage;
    private Integer pageSize;
    
    private String sortingField;
    private Boolean isAscending;
    
    /**
     * @return the pageCount
     */
    public int getEntityCount() {
        return entityCount;
    }

    /**
     * @param pageCount
     *            the pageCount to set
     */
    public void setEntityCount(int entityCount) {
        this.entityCount = entityCount;
    }

    /**
     * @return the currentPage
     */
    public Integer getCurrentPage() {
        return currentPage;
    }

    /**
     * @param currentPage
     *            the currentPage to set
     */
    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * @return the pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize
     *            the pageSize to set
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    
    public ApartmentStatus getStatus() {
        return status;
    }

    public void setStatus(ApartmentStatus status) {
        this.status = status;
    }
    
    public User getModerator() {
        return moderator;
    }

    public void setModerator(User moderator) {
        this.moderator = moderator;
    }
    
    public Boolean getPublish() {
        return publish;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }
    
    
    /**
     * @return the sortingField
     */
    public String getSortingField() {
        return sortingField;
    }

    /**
     * @param sortingField the sortingField to set
     */
    public void setSortingField(String sortingField) {
        this.sortingField = sortingField;
    }

    /**
     * @return the isAscending
     */
    public Boolean getIsAscending() {
        return isAscending;
    }

    /**
     * @param isAscending the isAscending to set
     */
    public void setIsAscending(Boolean isAscending) {
        this.isAscending = isAscending;
    }
}
