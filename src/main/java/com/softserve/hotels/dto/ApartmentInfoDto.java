package com.softserve.hotels.dto;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.softserve.hotels.model.ActionStatus;
import com.softserve.hotels.model.ApartmentStatus;
import com.softserve.hotels.model.User;

public class ApartmentInfoDto {

    private User renter;
    private Boolean published;

    private ApartmentStatus apartmentStatus;

    private int entityCount;
    private int currentPage;
    private int pageSize;

    private String name;

    // private ActionStatus actionStatus;

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public void setRenter(User renter) {
        this.renter = renter;
    }

    public User getRenter() {
        return renter;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setEntityCount(int entityCount) {
        this.entityCount = entityCount;
    }

    public int getEntityCount() {
        return entityCount;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setApartmentStatus(ApartmentStatus apartmentStatus) {

        this.apartmentStatus = apartmentStatus;
    }

    public ApartmentStatus getApartmentStatus() {
        return apartmentStatus;
    }

}
