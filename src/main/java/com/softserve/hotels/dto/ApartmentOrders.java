package com.softserve.hotels.dto;

import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

public class ApartmentOrders {
    
    private int entityCount;
    private int currentPage;
    private int pageSize;

    private String name;
    private String city;
    private String idCity;

    private Integer maxCountGuests;
    private String sortBy;
    private Integer paymentMethod;
    
    private Float startRaiting;
    private Float endRaiting;
    private Float startPrice;
    private Float endPrice;
    private List<Integer> conveniences;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }
    
    public Integer getMaxCountGuests() {
        return maxCountGuests;
    }

    public void setMaxCountGuests(Integer maxCountGuests) {
        this.maxCountGuests = maxCountGuests;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Float getStartRaiting() {
        return startRaiting;
    }

    public void setStartRaiting(Float startRaiting) {
        this.startRaiting = startRaiting;
    }

    public Float getEndRaiting() {
        return endRaiting;
    }

    public void setEndRaiting(Float endRaiting) {
        this.endRaiting = endRaiting;
    }

    public Float getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Float startPrice) {
        this.startPrice = startPrice;
    }

    public Float getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(Float endPrice) {
        this.endPrice = endPrice;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }


    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

 
    public int getCurrentPage() {
        return currentPage;
    }

  
    public void setEntityCount(int entityCount) {
        this.entityCount = entityCount;
    }

    public int getEntityCount() {
        return entityCount;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    
    public int getPageSize() {
        return pageSize;
    }

    public List<Integer> getConveniences() {
        return conveniences;
    }

    public void setConveniences(List<Integer> conveniences) {
        this.conveniences = conveniences;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
}
