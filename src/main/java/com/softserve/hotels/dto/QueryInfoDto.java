package com.softserve.hotels.dto;

import com.softserve.hotels.model.Role;

public class QueryInfoDto {

    private int entityCount;
    private Integer currentPage;
    private Integer pageSize;

    private String sorting;
    private Boolean asc;
    private Role role;
    private String email;
    


    public QueryInfoDto(Integer pageSize, Integer currentPage) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

    public QueryInfoDto(String sorting, Boolean asc) {
        this.sorting = sorting;
        this.asc = asc;
    }

    public QueryInfoDto(String sortingField, Boolean isAscending, Integer pageSize, Integer currentPage, String email, Role role) {
        this(sortingField, isAscending);
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.email = email;
        this.role = role;
    }

    /**
     * @return the pageCount
     */
    public int getEntityCount() {
        return entityCount;
    }

    /**
     * @param pageCount
     *            the pageCount to set
     */
    public void setEntityCount(int entityCount) {
        this.entityCount = entityCount;
    }

    /**
     * @return the currentPage
     */
    public Integer getCurrentPage() {
        return currentPage;
    }

    /**
     * @param currentPage
     *            the currentPage to set
     */
    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * @return the pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize
     *            the pageSize to set
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    
    /**
     * @return the sortingField
     */
    public String getSorting() {
        return sorting;
    }

    /**
     * @param sortingField the sortingField to set
     */
    public void setSorting(String sortingField) {
        this.sorting = sortingField;
    }

    /**
     * @return the isAscending
     */
    public Boolean getAsc() {
        return asc;
    }

    /**
     * @param isAscending the isAscending to set
     */
    public void setAsc(Boolean isAscending) {
        this.asc = isAscending;
	}

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    
    }
    
}
