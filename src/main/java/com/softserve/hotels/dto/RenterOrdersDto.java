package com.softserve.hotels.dto;

import com.softserve.hotels.model.ActionStatus;
import com.softserve.hotels.model.Apartment;
import com.softserve.hotels.model.ApartmentStatus;
import com.softserve.hotels.model.User;

public class RenterOrdersDto {
    private Apartment apartment;

    private User renter;
    
    private ActionStatus actionStatus;
    
    private Integer idReservation;

    private String comment;


    private int entityCount;
    private int currentPage;
    private int pageSize;

    // private ActionStatus actionStatus;


    public void setRenter(User renter) {
        this.renter = renter;
    }

    public User getRenter() {
        return renter;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setEntityCount(int entityCount) {
        this.entityCount = entityCount;
    }

    public int getEntityCount() {
        return entityCount;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setActionStatus(ActionStatus actionStatus) {

        this.actionStatus = (actionStatus == null)? ActionStatus.WAITING_CONFIRMATION: actionStatus;
    }

    public ActionStatus getActionStatus() {
        return actionStatus;
    }
    
    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public Integer getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(Integer idReservation) {
        this.idReservation = idReservation;
    }
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
}
