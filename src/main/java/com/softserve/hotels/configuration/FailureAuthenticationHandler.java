
package com.softserve.hotels.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.softserve.hotels.model.Role;
import com.softserve.hotels.model.User;
import com.softserve.hotels.service.UserService;

public class FailureAuthenticationHandler extends SimpleUrlAuthenticationFailureHandler {

	@Autowired
	private UserService userService;

	private User user;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {

		String email = request.getParameter("j_username");

		user = userService.findUserByEmail(email);

		String error = "invalidCredentials";
		if (user != null) {
			if (!user.isEnabled() && user.getRole() == Role.RENTER) {
				error = "renterNotConfirmed";
			} else if (!user.isEnabled()) {
				error = "loginNotConfirmed";
			} else if (user.getBlocked()) {
				error = "loginBlocked";
			}
		}

		this.setDefaultFailureUrl("/login?email=" + email +"&error=" + error);
		super.onAuthenticationFailure(request, response, exception);
	}

}