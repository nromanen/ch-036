/**
 * 
 */
function checkAppartment(apartment_id, arrivalDate, departureDate) {
    var result = null;
     var token = $("meta[name='_csrf']").attr("content");
     var csrfheader = $("meta[name='_csrf_header']").attr("content");
     var URL = /[/][A-z0-9\-\.]+[/]/.exec(window.location.pathname) + "tenant/bookApartment";

     $.ajax({
         url : URL,
         method : "POST",
         data : {
             "_csrf" : token,
             "apartment_id" : apartment_id,
             "startDate" : arrivalDate,
             "endDate" : departureDate
         },
         async: false

     }).success(function(e) {
         result = e;
         console.log(e); 
     }).fail(function(e) {
         console.log(e); 
     });
    
    return result;
}