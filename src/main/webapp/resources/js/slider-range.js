function defaultValueIfBlank(value, def) {
    return $.trim(value) !== "" ? value : def;
}

jQuery(document).ready(function(){

    //price
    jQuery("#slider-range-price").slider({
        min: 0,
        max: 500,
        values: [defaultValueIfBlank(jQuery("input#startPriceFilter").val(), 0), 
                 defaultValueIfBlank(jQuery("input#endPriceFilter").val(), 500)],
        step: 50,
        range: true,
        stop: function(event, ui) {
            jQuery("input#startPriceFilter").val(jQuery("#slider-range-price").slider("values",0));
            jQuery("input#endPriceFilter").val(jQuery("#slider-range-price").slider("values",1));  
        },
        slide: function(event, ui){
            jQuery("input#startPriceFilter").val(jQuery("#slider-range-price").slider("values",0));
            jQuery("input#endPriceFilter").val(jQuery("#slider-range-price").slider("values",1));
        }
    
    });

    jQuery("input#startPriceFilter").change(function() {
    
        var startPriceFilter=jQuery("input#startPriceFilter").val();
        var endPriceFilter=jQuery("input#endPriceFilter").val();
    
        if(parseInt(startPriceFilter) > parseInt(endPriceFilter)) {
            startPriceFilter = endPriceFilter;
            jQuery("input#startPrice").val(startPriceFilter);
        }
        jQuery("#slider-range-price").slider("values",0,startPriceFilter);    
    });

    jQuery("input#endPriceFilter").change(function(){
            
        var startPriceFilter=jQuery("input#startPriceFilter").val();
        var endPriceFilter=jQuery("input#endPriceFilter").val();
        
        if (endPriceFilter > 500) { endPriceFilter = 500; jQuery("input#endPriceFilter").val(500)}
    
        if(parseInt(startPriceFilter) > parseInt(endPriceFilter)){
            endPriceFilter = startPriceFilter;
            jQuery("input#endPriceFilter").val(endPriceFilter);
        }
        jQuery("#slider-range-price").slider("values",1,endPriceFilter);
    });
    
    jQuery('#startRaitingFilter,#endRaitingFilter,#startPriceFilter,#endPriceFilter,#maxCountGuestsFilter').keypress(function(event){
        var key, keyChar;
        if(!event) var event = window.event;
        
        if (event.keyCode) key = event.keyCode;
        else if(event.which) key = event.which;
    
        if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
        keyChar=String.fromCharCode(key);
        
        if(!/\d/.test(keyChar)) return false;
    
    });
    
    //rating
    jQuery("#slider-range-rating").slider({
        min: 0,
        max: 5,
        values: [defaultValueIfBlank(jQuery("input#startRaitingFilter").val(), 0), 
                 defaultValueIfBlank(jQuery("input#endRaitingFilter").val(), 500)],
        range: true,
        step: 0.5,
        stop: function(event, ui) {
            jQuery("input#startRaitingFilter").val(jQuery("#slider-range-rating").slider("values",0));
            jQuery("input#endRaitingFilter").val(jQuery("#slider-range-rating").slider("values",1));
            
        },
        slide: function(event, ui){
            jQuery("input#startRaitingFilter").val(jQuery("#slider-range-rating").slider("values",0));
            jQuery("input#endRaitingFilter").val(jQuery("#slider-range-rating").slider("values",1));
        }
    });

    jQuery("input#startRaitingFilter").change(function(){
    
        var startRaitingFilter=jQuery("input#startRaitingFilter").val();
        var endRaitingFilter=jQuery("input#endRaitingFilter").val();
    
        if(parseInt(startRaitingFilter) > parseInt(endRaitingFilter)){
            startRaitingFilter = endRaitingFilter;
            jQuery("input#startRaitingFilter").val(startRaitingFilter);
        }
        jQuery("#slider-range-rating").slider("values",0,startRaitingFilter);    
    });

    
    jQuery("input#endRaitingFilter").change(function(){
            
        var startRaitingFilter=jQuery("input#startRaitingFilter").val();
        var endRaitingFilter=jQuery("input#endRaitingFilter").val();
        
        if (endRaitingFilter > 5) { endRaitingFilter = 5; jQuery("input#endRaitingFilter").val(5)}
    
        if(parseInt(startRaitingFilter) > parseInt(endRaitingFilter)){
            endRaitingFilter = startRaitingFilter;
            jQuery("input#endRaitingFilter").val(endRaitingFilter);
        }
        jQuery("#slider-range-rating").slider("values",1,endRaitingFilter);
    });

});