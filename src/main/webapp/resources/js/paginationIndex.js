$(document).ready(function(){

    var dateFormat = "yy-mm-dd";
    
	$("a[btnIndex]").click(function() {
	    
	    var $elem = this.closest("li");
	    console.log($($elem).hasClass("disabled"));
        var index = $(this).attr("btnIndex");
        var $form = $("#paginationIndexForm");
        
        $("#pgCity").val($("#cityFilter").val());
        $("#pgIdCity").val($("#IdCityFilter").val());
        $("#pgStartDate").val($("#startDateFilter").val());
        $("#pgEndDate").val($("#endDateFilter").val());
        $("#pgStartPrice").val($("#startPriceFilter").val());
        $("#pgEndPrice").val($("#endPriceFilter").val());
        $("#pgStartRating").val($("#startRatingFilter").val());
        $("#pgEndRating").val($("#endRatingFilter").val());
        $("#pgName").val($("#nameFilter").val());
        $("#pgSortBy").val($("#sortByFilter").val());
        $("#pgPaymentMethod").val($("#paymentMethodFilter").val());
        
        console.log($form.attr("action") + index);
        $form.attr("action", $form.attr("action") + index);
        $form.submit();
    })
    
    $(function() {
        var spring_message = getMessages(["datePicker.localization"]);
        var local = spring_message["datePicker.localization"];
        
        $.datepicker.setDefaults( $.datepicker.regional[ local ] );
        
        $("#startDateFilter").datepicker(
                {
                    dateFormat : dateFormat
                    }
        );
        $("#endDateFilter").datepicker(
                {
                    dateFormat : dateFormat
                    }
        );
      });
	
	$("#idCity").val("");
})

function initializeAutocompleteCities() {
    var input = $('#cityFilter')[0];
    var options = {
      types: ['(regions)'],
      minLength: 2
    };

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace(); 
      $("#selected").val(1);
      $("#cityFilter").focus();
      var form = input.parentElement;
      $("#idCity").val(place.place_id);
    });

  }
  google.maps.event.addDomListener(window, 'load', initializeAutocompleteCities);

function resetFilterApartment() {
    $("#cityFilter").val("");
    $("#startDateFilter").val("");
    $("#endDateFilter").val("");
    $("#maxCountGuestsFilter").val("");
    $("#nameFilter").val("");
    $("#sortByFilter").val("sortByCity");
    $("#paymentMethodFilter").val("PayOnArrival");
    $("#startRaitingFilter").val("");
    $("#endRaitingFilter").val("");
    $("#startPriceFilter").val("");
    $("#endPriceFilter").val("");
    $("#slider-range-price").slider("values",0,0);
    $("#slider-range-price").slider("values",1,500);
    $("#slider-range-rating").slider("values",0,0);
    $("#slider-range-rating").slider("values",1,5);
    $('input:checkbox').removeAttr('checked');
    submitForm();
}

function validateFilterApartment() {
    var startDate = document.getElementById('startDateFilter');
    var endDate = document.getElementById('endDateFilter');
    
    if(endDate.value < startDate.value) {
        $("#modalFilterMessage").modal("show");
    } else {
        submitForm();
    }
}

function submitForm() {
    $('#filterApartment').submit();
}