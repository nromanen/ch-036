<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
    uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<header class="bg-info">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1>
                    <a href="<c:url value='/' />" class="logo">
                        <span class="glyphicon glyphicon-map-marker"></span>
                        Booking
                    </a>
                    <small><em><spring:message
                                code="header.motto" /></em></small>
                </h1>
            </div>
            <div class="col-md-3 text-right user-control-wrapper">
                <sec:authorize access="isAnonymous() ">
                    <a class="btn btn-info btn-xs"
                        href="<c:url value="/login" />">
                        <span class="glyphicon glyphicon-log-in">
                        </span>
                        <spring:message code="login" />
                    </a>
                    <a class="btn btn-info btn-xs"
                        href="<c:url value="/registration" />">
                        <span class="glyphicon glyphicon-download"></span>
                        <spring:message code="header.registration" />
                    </a>
                </sec:authorize>
                <sec:authorize access="isAuthenticated() ">
                    <c:url var="userLink" value='/userSettings' />
                    <sec:authorize access="hasAuthority('USER')">
                        <c:url var="userLink"
                            value="/tenant/activeOrders/" />
                    </sec:authorize>
                    <sec:authorize access="hasAuthority('RENTER')">
                        <c:url var="userLink"
                            value="/renter/allApartments" />
                    </sec:authorize>
                    <sec:authorize access="hasAuthority('MODERATOR')">
                        <c:url var="userLink"
                            value="/moderator/enabledApartments" />
                    </sec:authorize>
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <c:url var="userLink" value="/admin/allUsers" />
                    </sec:authorize>
                    <a href="#" class="dropdown-toggle btn btn-sm user-dropdown"
                        data-toggle="dropdown" role="button"
                        aria-haspopup="true" aria-expanded="false">
                    <span>
                        <spring:message code="header.hello" />
                        ,
                    </span>
                
                    
                        <sec:authentication
                            property="principal.username" />
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${userLink}">
                                <span
                                    class="glyphicon glyphicon-dashboard"></span>
                                <span class="dashboard-span">Dashboard</span>
                            </a></li>
                        <li><a class="user-settings"
                                href="<c:url value='/userSettings' />">
                                <span class="glyphicon glyphicon-user"></span>
                                <span class="settings-span">
                                    <spring:message
                                        code="userMenu.profile" />
                                </span>
                            </a></li>
                        <li role="separator" class="divider"></li>
                        <li><c:url var="logoutUrl" value="/logout" />
                            <form:form action="${logoutUrl}"
                                method="post">
                                <button class="logout-btn btn" type="submit">
                                    <span
                                        class="glyphicon glyphicon-log-out">
                                    </span>
                                    <spring:message code="header.logout" />
                                </button>
                            </form:form></li>
                    </ul>
                </sec:authorize>
            </div>
            <div class="col-md-12 languages">
                <a href="en">
                    <div class="flag flag-gb" alt="English"></div>
                </a>
                |
                <a href="ua">
                    <div class="flag flag-ua" alt="Ukrainian"></div>
                </a>
            </div>
        </div>
    </div>
    <script>
                    $(document)
                            .ready(
                                    function() {
                                        $(".languages a")
                                                .click(
                                                        function(event) {
                                                            event
                                                                    .preventDefault();
                                                            $
                                                                    .each(
                                                                            sessionStorage,
                                                                            function(
                                                                                    key,
                                                                                    value) {
                                                                                if (key
                                                                                        .startsWith("locale.")) {
                                                                                    sessionStorage
                                                                                            .removeItem(key);
                                                                                }
                                                                            });
                                                            var parameterName = "language";
                                                            var parameterValue = $(
                                                                    this).attr(
                                                                    "href");
                                                            window.location.href = addParameter(
                                                                    window.location.href,
                                                                    parameterName,
                                                                    parameterValue);

                                                        });
                                    });
                </script>
</header>