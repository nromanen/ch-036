<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="footer-placeholder"></div>
<footer class="bg-info">
    <div class="container">
        <div>
            <h4>
                <label class="tooltip_"><spring:message code="footer.authors" />
                (Ch-036)</label>
            </h4>
                <label class="tooltip_">Bogdan Gats, 
                <span><img class="image-avatar-authors" src='<c:url value="/img/authors/Gats.jpg" />' /></span></label>
                <label class="tooltip_">Olexandr Huldin, 
                <span><img class="image-avatar-authors" src='<c:url value="/img/authors/Huldin.jpg" />' /></span></label>
                <label class="tooltip_">Mykola Ilashchuk 
                <span><img class="image-avatar-authors" src='<c:url value="/img/authors/Ilashckuk.png"/>' /></span></label> <br>
                <label class="tooltip_">Serhiy Makhov, 
                <span><img class="image-avatar-authors" src='<c:url value="/img/authors/Makhov.png"/>' /></span></label>
                <label class="tooltip_">Volodymyr Rogulya, 
                <span><img class="image-avatar-authors" src='<c:url value="/img/authors/Rogulya.png"/>' /></span></label> 
				<label class="tooltip_">Bogdan Shtepuliak
				<span><img class="image-avatar-authors" src='<c:url value="/img/authors/Shtepuliak.png"/>' /></span></label>
		</div>
        <div class="col-md-12 text-right">
            <h6>
                
                <spring:message code="footer.contactUs" />: 
                <a href="mailto:booking.it.university@gmail.com"><span class="glyphicon glyphicon-envelope"></span> booking.it.university@gmail.com</a>
            </h6>
            <h6 class="rights">
                <spring:message code="footer.rightsReserved" /> &copy;
            </h6>
        </div>
    </div>
</footer>