<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ul class="list-unstyled menu-list">
    <c:if test="${apartment.status eq 'ENABLED'}">
    <%-- <li><a href="<c:url value = "/renter/renterOrders/${apartment.getId()}"/>" filterHader="ALL">
         <spring:message code="apartmentDetail.allReservations" />
     </a></li>     --%>
    <li><a href="#" filterHader="WAITING_CONFIRMATION" class="FilterMenu">
            <spring:message code="apartmentDetail.needConfirmReservation" />
        </a></li>
<%--     <li><a href="#' filterHader="WAITING_PAYMENT" class="FilterMenu">
            <spring:message code="apartmentDetail.futureReservation" />
        </a></li> --%>
     <li><a href="#" filterHader="WAITING_PAYMENT" class="FilterMenu">
            <spring:message code="ActionStatus.WAITING_PAYMENT" />
        </a></li>
    <li><a href="#" filterHader="APROVED" class="FilterMenu">
            <spring:message code="ActionStatus.APROVED" />
        </a></li>        
    <li><a href="#" filterHader="ALL_INACTIVE" class="FilterMenu">
            <spring:message code="apartmentDetail.history" />
        </a></li>    
    </c:if>
</ul>