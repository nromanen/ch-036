<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ul class="list-unstyled menu-list">
    <li><a href="<c:url value='/renter/allApartments' />">
            <spring:message code="moderatorMap.allApartments" />
        </a></li>
    <li><a href="#" class="FilterMenu"  filterHader='ENABLED' publishHader = 'true'>
            <spring:message code="allApartments.enabledApartments" />
        </a></li>
    <li><a href="#" class="FilterMenu" filterHader='ENABLED' publishHader = 'false'>
            <spring:message code="allApartments.unpublishedApartments" />
        </a></li>
    <li><a href="#" class="FilterMenu" filterHader='DISABLED' publishHader = ''>
            <spring:message code="allApartments.disabledApartments" />
        </a></li>
</ul>