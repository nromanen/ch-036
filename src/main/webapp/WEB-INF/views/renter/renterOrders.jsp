<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="nav" uri="/WEB-INF/views/ordersTag.tld"%>
<div class="modal fade" tabindex="-1" role="dialog" id="modalConfirmDeleteReservation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <spring:message code="apartmentDetail.modalTitleDeleteRegistration" />
                </h4>
            </div>
            <c:url var="urlRemoveReservationByRenter" value='/renter/removeReservationByRenter' />
            <form:form action="${urlRemoveReservationByRenter}" id="frmDeleteReservation" method="post"
                modelAttribute="renterOrdersDto">
                <div class="modal-body">
                    <form:hidden name="idReserv" id="frmIdReserv" path="idReservation" value=""/>
                    <form:hidden path="actionStatus" value=""/>
                    <form:hidden path="pageSize" value="" />
                    <form:hidden path="currentPage" value=""/>
                    <spring:message code="apartmentDetail.deletingReservationComment" />
                    <form:textarea rows="3" cols="" maxlength="150" id="frmComment" name="comment" class="form-control"
                        path="comment" />
                </div>
                <div class="modal-footer">
                    <form:button type="button" class="btn btn-default" data-dismiss="modal">
                        <spring:message code="apartmentDetail.dontConfirm" />
                    </form:button>
                    <form:button type="submit" class="btn btn-primary" id="confirmDeleteRegistration">
                        <spring:message code="apartmentDetail.confirm" />
                    </form:button>
                </div>
            </form:form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalBoxInfo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <spring:message code="apartmentDetail.modalTitle" />
                </h4>
            </div>
            <div class="modal-body">
                <c:if test="${not empty info && info.event == 'deleteReservation'}">
                    <spring:message code="apartmentDetail.messageDeleteReservation" />
                </c:if>
                <c:if test="${not empty info && info.event == 'confirmReservation'}">
                    <spring:message code="apartmentDetail.messageConfirmReservation" />
                </c:if>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <spring:message code="close" />
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="futureReservations">
    <c:if test="${not empty info}">
        <input type="hidden" name="infoEvent" id="infoEvent" value="${info.event}">
    </c:if>
    <c:if test="${empty info}">
        <input type="hidden" name="infoEvent" id="infoEvent" value="">
    </c:if>
    <div class="page-header">
        <h3>
            <c:if test="${apartment.aproved == true}">
            </c:if>${apartment.name}
        </h3>
    </div>
    <c:if test="${empty renterOrders}">
        <spring:message code="apartmentDetail.noReserved" />
    </c:if>
    <c:if test="${not empty renterOrders}">
        <div class="table-cst">
            <table class="table table-striped status-apartments-table">
                <tr>
                    <th><spring:message code="apartmentDetail.Reservation.Start" /></th>
                    <th><spring:message code="apartmentDetail.Reservation.End" /></th>
                    <th><spring:message code="apartmentDetail.Reservation.User" /></th>
                    <th><spring:message code="confirm" /></th>
                    <th><spring:message code="cancel" /></th>
                </tr>
                <c:forEach items="${renterOrders}" var="reserv">
                    <tr>
                        <td>${reserv.dateStartReservation}</td>
                        <td>${reserv.dateEndReservation}</td>
                        <td><a href="<c:url value='/renter/userDetails/${reserv.tenant.id}/${apartment.id}'/>">
                                ${reserv.tenant.nickname} </a></td>
                        <td><c:choose>
                                <c:when test="${reserv.status eq 'WAITING_CONFIRMATION'}">
                                    <a href="<c:url value='/renter/confirmReservation/${reserv.id}'/>"
                                        class="confirmReservation" data-toggle="tooltip" data-placement="top"
                                        title="<spring:message code="confirm"/>"> <span
                                        class="glyphicon glyphicon-ok"></span>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <span class="glyphicon glyphicon-ok"></span>
                                </c:otherwise>
                            </c:choose></td>
                        <td><c:choose>
                                <c:when test="${nav:isActive(reserv.status) == true or nav:isPayment(reserv.status) == true}">
                                    <a href="#" appId="${reserv.id}" class="renterDeleteReservation" data-toggle="tooltip"
                                        data-placement="top" title="<spring:message code="cancel"/>"> <span
                                        class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <span class="glyphicon glyphicon-remove"></span>
                                </c:otherwise>
                            </c:choose></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </c:if>
</div>


<!-- 
   Pagination
 -->
<c:url var="paginationLink" value="/renter/renterOrders/${apartment.id}" />
<form:form id="paginationForm" action="${paginationLink}" modelAttribute="renterOrdersDto" method="GET"
    style="display: none">
    <form:hidden path="actionStatus" value="" id="statusFilter" />
    <form:hidden path="pageSize" value="" />
    <form:hidden path="currentPage" value="" id="pageNumber" />
</form:form>

<nav:paginationTag currentPage="${renterOrdersDto.currentPage}" pageSize="${renterOrdersDto.pageSize}"
    entityCount="${renterOrdersDto.entityCount}" />

<script>
    $("a.FilterMenu[filterHader]").click(function() {
        var $form = $("#paginationForm");
        $("#statusFilter").val($(this).attr("filterHader"));
        console.log($(this).attr("filterHader"));
        $form.submit();
    })

    $(document).ready(function() {
        var filterStatus = "${renterOrdersDto.actionStatus}";
        if ($("a[filterHader=" + filterStatus + "]").length) {
            $(".menu-list li").removeClass("active");
            $("a[filterHader=" + filterStatus + "]").closest("li").addClass("active");
        }
    });
</script>