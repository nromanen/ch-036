<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" uri="/WEB-INF/views/ordersTag.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- Modals -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalPublishBad">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header-red modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">
     <spring:message code="apartmentDetail.modalTitle" />
    </h4>
   </div>
   <div class="modal-body">
    <p>
     <spring:message code="apartmentDetail.changesFailed" />
    </p>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">
     <spring:message code="close" />
    </button>
   </div>
  </div>
  <!-- /.modal-content -->
 </div>
 <!-- /.modal-dialog -->
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modalPublishGood">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">
     <spring:message code="apartmentDetail.modalTitle" />
    </h4>
   </div>
   <div class="modal-body">
    <p>
          Are you sure?
    </p>
   </div>
   <div class="modal-footer">
    <a href="<c:url value ='/renter/allApartments?apartmentStatus=ENABLED&published=true'/>" class="btn btn-primary" >
      Accept
    </a>
    <button type="button" class="btn btn-default" data-dismiss="modal">
     <spring:message code="close" />
    </button>
   </div>
  </div>
  <!-- /.modal-content -->
 </div>
 <!-- /.modal-dialog -->
</div>


<div>
 <div class="row">
 <div class="col-md-8 centering-box"> 
  <h4>
    <spring:message code="moderatorMap.allApartments" />
  </h4>
  </div>
  
  <div class="col-md-4" align="center">
  <a type="button" href="<c:url value='/renter/formApartment' />" class="btn btn-primary" >
    <span class="glyphicon glyphicon-plus"></span>
    <spring:message code="allApartments.addApartment" />
  </a>
  </div>
  </div>
 <c:if test="${empty listApartments}">
   <div class="row text-center">
    <spring:message code="allApartments.noApartments" />
  </div>
 </c:if>
 <c:if test="${!empty listApartments}">
  <table class="table table-striped status-apartments-table">
   <tr>
    <th width="120"><spring:message code="apartment.name" /></th>
    <th width="30"><spring:message code="allApartments.detail" /></th>
    <th width="30"><spring:message code="apartmentDetail.Reservation.Status" /></th>
    <th width="30"><spring:message code="MODERATOR" /></th>
    <th width="30"><spring:message code="apartmentDetail.orders" /></th>
    <th width="10"><spring:message code="moderator.approved" /></th>
   </tr>
   <c:forEach items="${listApartments}" var="apartment">
    <tr>

     <td><c:if test="${apartment.status eq 'ENABLED'}">
       <a href="<c:url value='/renter/preview/${apartment.getId()}' />"> 
      </c:if> ${apartment.getName()} <c:if test="${apartment.status eq 'ENABLED'}">
       </a>
      </c:if></td>
     <td><a href="<c:url value='/renter/apartmentDetail/${apartment.id}' />"> <span
       class="glyphicon glyphicon-pencil"></span>
     </a></td>
     <td><c:choose>
       <c:when test="${apartment.status != 'ENABLED'}">
        <span class="glyphicon glyphicon-ban-circle"></span>
       </c:when>
       <c:when test="${apartment.published}">
        <a href="<c:url value='/renter/disableApartment/${apartment.id}' />"> <span
         class="glyphicon glyphicon-eye-close"></span>
        </a>
       </c:when>
       <c:when test="${!apartment.published}">
        <a href="#" getValue="<c:url value='/renter/publishApartment/${apartment.id}' />"> <span
         class="glyphicon glyphicon-eye-open"></span>
        </a>
       </c:when>

      </c:choose></td>
     <td><c:if test="${apartment.moderator != null}">
       <a href="<c:url value='/renter/userDetails/${apartment.moderator.id}/${apartment.id}'/>">
        ${apartment.moderator.nickname } </a>
      </c:if> <c:if test="${apartment.moderator == null}">
       <span class="glyphicon glyphicon-minus"></span>
      </c:if></td>
     <td><a href="<c:url value='/renter/renterOrders/${apartment.id}'/>"> <span
       class="glyphicon glyphicon-time"></span>
     </a></td>
     <td><c:if test="${apartment.aproved}">
       <span class="glyphicon glyphicon-ok approved-ico"></span>
      </c:if></td>
    </tr>
   </c:forEach>
  </table>
 </c:if>
 <hr>
</div>

<c:url var="paginationLink" value="/renter/allApartments" />
<form:form id="paginationForm" action="${paginationLink}" modelAttribute="apartmentInfoDto" method="GET"
 style="display: none">
 <form:hidden path="pageSize" value="" />
 <form:hidden path="currentPage" value=""  id="pageNumber"/>
 <form:hidden path="apartmentStatus" value="" id="statusFilter" />
 <form:hidden path="published" value="" id="publishFilter" />
</form:form>

  <nav:paginationTag currentPage="${apartmentInfoDto.currentPage}" pageSize="${apartmentInfoDto.pageSize}"
   entityCount="${apartmentInfoDto.entityCount}" />



<script>
    $("a.FilterMenu[filterHader]").click(function() {
        var $form = $("#paginationForm");
       if($("#statusFilter").val() != $(this).attr("filterHader")){ 
        $("#statusFilter").val($(this).attr("filterHader"));
        $("#pageNumber").val("1");
       }
       if(  $("#publishFilter").val() != $(this).attr("publishHader")){
        $("#publishFilter").val($(this).attr("publishHader"));
        $("#pageNumber").val("1");
       }
        $form.submit();
    });
    
    $("a[getValue]").click(function() {
        console.log("${apartment.id}");
        var res = publishPossible($(this).attr("getValue"));
        if(res == "Done") {
            $("#modalPublishGood").modal('show') ;
        }
        else {
            $("#modalPublishBad").modal('show') ; 
        }
       
    });
    

    $(document).ready(
         function() {
                var statusFilter = "${apartmentInfoDto.apartmentStatus}";
                var publishFilter = "${apartmentInfoDto.published}";

                if (statusFilter != '' && publishFilter != '') {
                    $(".menu-list li").removeClass("active");
                    $("a[filterHader= " + statusFilter + "][publishHader=" + publishFilter + "]").closest("li")
                            .addClass("active");                    
                }

                else if (statusFilter != '') {
                    $(".menu-list li").removeClass("active");
                    $("a[filterHader= " + statusFilter + "]").closest("li").addClass("active");
                }
            });
    
    function publishPossible(url) {
        var result;
        var token = $("meta[name='_csrf']").attr("content");
    	 $.ajax({
             url : url,
             method : "POST",
             data : {
                 "_csrf" : token
             },
             async: false

         }).success(function(e) {
             result = e;
             console.log(e); 
         }).fail(function(e) {
             console.log(e); 
         });
    	
    	return result;
    }
</script>