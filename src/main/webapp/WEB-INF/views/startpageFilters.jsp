<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel-primary filtetableApart">
	<table class="table">
		<thead>
			<c:url var="filterLink" value="/1" />
			<form:form modelAttribute="filterApartment" method="post"
				id="filterApartment" action="${filterLink}" name="filterApartment">
				<form:hidden path="idCity" />
				<spring:message var="city" code="apartmentOrders.city"/>
				<spring:message var="startDate" code="apartmentOrders.startDate"/>
				<spring:message var="endDate" code="apartmentOrders.endDate"/>
				<spring:message var="maxCountGuests"
					code="apartmentOrders.maxCountGuests" />
				<spring:message var="name" code="apartmentOrders.name" /><br>
				<tr class="filtersTr">
					<button type="button" onclick="validateFilterApartment();"
						class="btn btn-primary" id="SearchBtn" style="width: 70%;">
						<spring:message code="apartmentOrders.search" />
					</button>
					<button type="button"
						class="btn btn-primary form-control_ glyphicon glyphicon-remove"
						id="cleanFilter" onclick="return resetFilterApartment();"
						style="width: 30%;">
					</button>
				</tr><br><br>
				<tr class="filtersTr">
					<form:input class="form-control filter" placeholder="${city}"
						path="city" id="cityFilter"/>
				</tr><br>
				<tr class="filtersTr">
					<form:input class="form-control filter" placeholder="${startDate}"
						path="startDate" id="startDateFilter" />
				</tr><br>
				<tr class="filtersTr">
					<form:input class="form-control filter datepicker"
						placeholder="${endDate}" path="endDate" id="endDateFilter" />
				</tr><br>
				<tr class="filtersTr">
					<form:input type="number" class="form-control bfh-number"
						placeholder="${maxCountGuests}" path="maxCountGuests"
						id="maxCountGuestsFilter" min="1" max="10" step="1"/>
				</tr><br>
				<tr class="filtersTr">
					<form:input class="form-control filter" placeholder="${name}"
						path="name" id="nameFilter" />
				</tr><br>
				<tr class="filtersTr">
					<form:label path="startPrice">
						<spring:message code="apartmentOrders.price" />:
					</form:label><br>
					<form:input class="form-control_" path="startPrice" placeholder="0"
						id="startPriceFilter" /> -
					<form:input class="form-control_" path="endPrice" placeholder="500"
						id="endPriceFilter" /><br>
					<div id="slider-range-price"></div>
				</tr><br>
				<tr class="filtersTr">
					<form:label path="startRaiting">
						<spring:message code="apartmentOrders.rating" />:
					</form:label><br>
					<form:input class="form-control_" path="startRaiting"
						placeholder="0" id="startRaitingFilter" /> -
					<form:input class="form-control_" path="endRaiting" placeholder="5"
						id="endRaitingFilter" />
					<div id="slider-range-rating"></div>
				</tr><br>
				<tr class="filtersTr">
					<form:label path="paymentMethod">
						<spring:message code="apartmentOrders.paymentMethods" />:
					</form:label><br>
					<form:select class="form-control filter" path="paymentMethod"
						id="paymentMethodFilter">
						<c:forEach items="${paymentsMethod}" var="method">
							<option value="${method.id}">
								${method.name}	
							</option>
						</c:forEach>
					</form:select>
				</tr><br>
				<tr class="filtersTr">
					<div class="conveniences">
                        <form:label path="conveniences">
                        	<spring:message code="apartmentOrders.convenience" />:
                        </form:label><br> 
                        <c:forEach items="${conviniances}" var="convenience">
                        	<label><form:checkbox path="conveniences" value="${convenience.id}" />
							${convenience.name}</label><br>
						</c:forEach>
                    </div>
				</tr><br>
				<tr class="filtersTr">
					<form:label path="sortBy">
						<spring:message code="apartmentOrders.orderBy" />
					</form:label>
					<form:select class="form-control filter" path="sortBy"
						id="sortByFilter">
						<option value="sortByCityASC" ${ filterApartment.sortBy eq 'sortByCityASC' ? 'selected' : '' }>
							<spring:message code="apartmentOrders.orderBy.cityASC" />
						</option>
						<option value="sortByPriceASC" ${ filterApartment.sortBy eq 'sortByPriceASC' ? 'selected' : '' }>
							<spring:message code="apartmentOrders.orderBy.priceASC" />
						</option>
						<option value="sortByRatingASC" ${ filterApartment.sortBy eq 'sortByRatingASC' ? 'selected' : '' }>
							<spring:message code="apartmentOrders.orderBy.ratingASC" />
						</option>
						<option value="sortByCityDESC" ${ filterApartment.sortBy eq 'sortByCityDESC' ? 'selected' : '' }>
							<spring:message code="apartmentOrders.orderBy.cityDESC" />
						</option>
						<option value="sortByPriceDESC" ${ filterApartment.sortBy eq 'sortByPriceDESC' ? 'selected' : '' }>
							<spring:message code="apartmentOrders.orderBy.priceDESC" />
						</option>
						<option value="sortByRatingDESC" ${ filterApartment.sortBy eq 'sortByRatingDESC' ? 'selected' : '' }>
							<spring:message code="apartmentOrders.orderBy.ratingDESC" />
						</option>
					</form:select>
				</tr>
			</form:form>
		</thead>
	</table>
</div>