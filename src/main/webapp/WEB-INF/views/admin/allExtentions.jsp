<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<c:url var="urlUpdateSettings" value="/admin/deleteExtention" />
<c:url var="urlAddSettings" value="/admin/addExtention" />

<!-- dialogs -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalBoxInfo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <c:if test="${empty info || info.description eq ''}">
                        <spring:message code="apartmentDetail.modalTitle" />
                    </c:if>
                </h4>
            </div>
            <div class="modal-body">
                <c:if test="${not empty info && info.event eq 'notUpload'}">
                    <spring:message code="apartmentDetail.messagePhotoNotUpload" />
                </c:if>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <spring:message code="close" />
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<c:if test="${empty photoExtentions}">
    <h3><b><spring:message code="adminExtention.noExtention" /></b></h3>
    <table class="table table-striped admin-table">
            <tr class="col-md-3">
            <td><form:form name="allExtentions" action="${urlAddSettings}" modelAttribute="addExt"
                    class="allExtentions" method="POST" onsubmit="return validate();">
                    <div class="form-group">
                    <form:label path="extention" class="col-sm-2 control-label">
                        <spring:message code="adminExtention.createExtention" />
                    </form:label>                   
                        <div class="divInput">
                            <form:input path="extention" class="form-control" id="extention" />
                        </div>
                        <form:errors path="extention" cssClass="error" />
                        </div>
                    
                    <tr>
                        <td><button type="submit" class="btn btn-primary" id="imageSelectBtn">
                                <spring:message code="save" />
                            </button></td>
                    </tr>
                </form:form></td>
        </tr>
        </table>
</c:if>
<c:if test="${!empty photoExtentions}">
    <table class="table table-striped admin-table half-table">
        <tr>
            <th class="col-md-3" class="fixed"><spring:message code="adminExtention.allExtentions" /></th>
            <th class="col-md-1"><spring:message code="adminExtention.action" /></th>
        </tr>
        <c:forEach var="photo" items="${photoExtentions}">
            <tr>
                <td>${photo.extention}</td>
                
                <td><form:form name="allExtentions" action="${urlUpdateSettings}" modelAttribute="deleteExt"
                        method="POST">
                        <form:hidden path="id" value="${photo.id}" />
                        <form:hidden path="extention" value="${photo.extention}" />
                        <button type="submit" class="btn btn-default" id="imageSelectBtn">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            <%-- <spring:message code="delete" /> --%>
                        </button>
                    </form:form></td>
            </tr>

        </c:forEach>
        <tr>
            <td><form:form name="allExtentions" action="${urlAddSettings}" modelAttribute="addExt"
                    class="allExtentions" method="POST" onsubmit="return validate();">
                    <form:label path="extention">
                        <spring:message code="adminExtention.createExtention" />
                    </form:label>
                    <div class="form-group">
                        <div class="divInput">
                            <form:input path="extention" class="form-control" id="extention"></form:input>
                        </div>
                        <form:errors path="extention" cssClass="error" />
                    </div>
                    <button type="submit" class="btn btn-primary" id="imageSelectBtn">
                                <spring:message code="save" />
                            </button>
                </form:form></td>
        </tr>
    </table>
</c:if>

<!-- <script>
	if ($("#infoEvent").val() != '') {
		$("#modalBoxInfo").modal('show');
	}
</script> -->