<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="dump">
	<h3>Backup</h3>
    <div>
        <ol>
            <li>After completing data backup you will have zip
             archive in a server root - <code>ExtractMeInServerRoot.zip</code>.</li>
            <li>To decompress your backup, please unzip archive to the root folder of your server.</li>
            <li>Put <code>*.backup</code> and <code>windowsDBRecover.bat</code> (for users using servers on windows) 
            or <code>linuxDBRecover.sh</code> (for linux server users) to the same folder and run them (you can find this files at the bitbucket repo).</li>
            <li>Follow instructions.</li>
        </ol>
    </div>
    <a href="<c:url value='/admin/fullBackup'/>" class="btn btn-primary">Create data backup</a>
</div>