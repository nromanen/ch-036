<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<c:url var="urlUpdateSettings" value="/admin/adminConfiguration" />

        <div class="form-horizontal">
  
            <form:form name="adminConfiguration" action="${urlUpdateSettings}" modelAttribute="dto" method="GET"
                 onsubmit="return validate();" id="adminConfiguration">
   <c:forEach items="${dtoList}" var="valTest">            
                 <tr>
                    <td>
                    <form:label path="features" value="${valTest.features}">
                            <spring:message code="adminConfiguration.${valTest.features}" />
                        </form:label></td>
                    <td>
                    <form:hidden path="features" value="${valTest.features}" />    
                   </td>

    
                    
                    <div class="form-group" >
                    <div class="col-xs-2">
                    <div class="divInputContainer">
                    <form:input path="parameter" class="form-control" value="${valTest.parameter}" id="${valTest.features}"/>
                    </div>
                    </div>
                    </div>
                                                     
</c:forEach>  
                    <div class="form-group" >
                    <form:button type="submit" class="btn btn-primary" style="margin-left: 50px;">
                            <spring:message code="UserList.save" />
                    </form:button>
                    </div> 
                    
            </form:form>

        </div>

