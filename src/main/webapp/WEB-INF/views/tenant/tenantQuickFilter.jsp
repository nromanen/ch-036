<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<ul class="list-unstyled menu-list">
       <li><a href="<c:url value = "/tenant/activeOrders"/>" filterHader="ALL">
                <spring:message code="userMenu.ordersAll" />
            </a></li>
        <li><a href="#" filterHader="ALL_ACTIVE" class="FilterMenu">
                <spring:message code="userMenu.ordersActive" />
            </a></li>
        <li><a href="#" filterHader="WAITING_PAYMENT" class="FilterMenu">
                <spring:message code="userMenu.ordersToPay" />
            </a></li>
        <li><a href="#" filterHader="ALL_INACTIVE" class="FilterMenu">
                <spring:message code="userMenu.ordersInactive" />
            </a></li>
</ul>