<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="nav" uri="/WEB-INF/views/ordersTag.tld"%>

<div class="modal fade" tabindex="-1" role="dialog" id="modalFilterMessage">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <spring:message code="message" />
                </h4>
            </div>
            <div class="modal-body">
                <spring:message code="apartmentOrders.incorrectSearch" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <spring:message code="close" />
                </button>
            </div>
        </div>
    </div>
</div>

<c:set var="action" value="book" />
<sec:authorize access="hasAuthority('RENTER')">
    <sec:authentication property="principal.id" var="principalId" />
</sec:authorize>

<div class="panel-primary table-cst">
    <table class="table">
		<c:if test="${not empty apartmentList}"><br>
            <c:forEach items="${apartmentList}" var="apartment">
                <tr>
                    <c:set var="action" value="book" />
                    <sec:authorize access="hasAuthority('RENTER')">
                        <c:if test="${apartment.renter.id eq principalId}">
                            <c:set var="action" value="renter/apartmentDetail" />
                        </c:if>
                    </sec:authorize>
                    <div class="well row index-well">
                        <a href="<c:url value='/${action}/${apartment.getId()}' />">
                            <div class="index-apartment-image-wrapper col-md-2">
                                <c:if test="${not empty apartment.links}">
                                    <img class="img-rounded" src="<c:url value="Apartments/photo/${apartment.links.iterator().next().id}" />" />
                                </c:if>
								<div>
									<spring:message code="price" />: ${apartment.price}
									<spring:message code="UAH" />
								</div>
							</div>
                            <div class="index-apartment-info-wrapper col-md-7">
                                <div class="apartment-rating rating row">
                                    <div class="posted-rating-wrapper">
                                        <div class="posted-rating-value"></div>
                                        <div></div>
                                    </div>
                                    <span> <fmt:formatNumber type="number" maxFractionDigits="2"
                                            value="${apartment.raiting}" />
                                    </span>
								</div>
                                <h4>
                                    <c:if test="${apartment.aproved == true}">
                                        <span class="glyphicon glyphicon-ok approved-ico" data-toggle="tooltip"
                                            data-placement="top" title="<spring:message code='moderator.approved'/>"></span>
                                    </c:if>${apartment.name}</h4>
                                <div class="description">
                                    <span>${apartment.description}</span>
                                </div>
                                <div>
                                    <spring:message code="city" />
                                    : ${apartment.city}, 
                                    <spring:message code="address" />
                                    : ${apartment.address}
                                </div>
                                <div>
                                    <spring:message code="maxCountGuests" />
                                    : ${apartment.maxCountGuests}
                                </div>
								<div>
									<spring:message code="bookingApartment.convenience" />:
									<c:forEach items="${apartment.apartmentConveniences}"
										var="convenience">
										<c:if test="${convenience.exists == true }">
											<span class="badge badge-cst_">
												"${convenience.convenience.name}" </span>
										</c:if>
									</c:forEach>
								</div>
                            </div>
                            <div class="index-apartment-info-wrapper col-md-3"> 
								<spring:message code="adminConfiguration.payment" />
								:${payment}<br>
								<c:forEach items="${apartment.apartmentPayments}" var="payment">
									<c:if test="${payment.paymentMethod.enabled eq true && payment.exists eq true}">
										<span data-toggle="tooltip" data-placement="top"
											title="<spring:message code='orderPayment.${payment.paymentMethod.name}'/>">
											<input id="inputImage" name="imageLink"
											class="form-control image-cst_"
											src="${payment.paymentMethod.icon}" type="image">
										</span>
									</c:if>
								</c:forEach>
							</div>
						</a>
                    </div>
                </tr>
            </c:forEach>
        </c:if>
        <c:if test="${empty apartmentList}">
            <tr>
                <td></td>
                <td align="center"><spring:message code="apartmentList.nullSearchResult" /></td>
                <td></td>
            </tr>
        </c:if>
    </table>
</div>

<c:url var="paginationLink" value="/" />
<form:form id="paginationIndexForm" action="${paginationLink}" modelAttribute="filterApartment" method="post"
    style="display: none">
    <form:hidden path="city" value="" id="pgCity" />
    <form:hidden path="startDate" value="" id="pgStartDate" />
    <form:hidden path="endDate" value="" id="pgEndDate" />
    <form:hidden path="startPrice" value="" id="pgStartPrice" />
    <form:hidden path="endPrice" value="" id="pgEndPrice" />
    <form:hidden path="startRaiting" value="" id="startRatingFilter" />
    <form:hidden path="endRaiting" value="" id="endRatingFilter" />
    <form:hidden path="maxCountGuests" value="" id="pgMaxCountGuests" />
    <form:hidden path="name" value="" id="pgName" />
    <form:hidden path="sortBy" value="" id="pgSortBy" />
    <form:hidden path="paymentMethod" value="" id="pgPaymentMethod" />
</form:form>

<nav:paginationTag currentPage="${filterApartment.currentPage}" pageSize="${filterApartment.pageSize}"
    entityCount="${filterApartment.entityCount}" />

